.. index::
   pair: Pour une gauche démocratique et internationaliste ; 2023-12-10
   pair: Campisme; Pour une gauche démocratique et internationaliste, Contribution au renouveau et à la transformation de la gauche (2023-12-10)
   pair: Polarisation; Pour une gauche démocratique et internationaliste, Contribution au renouveau et à la transformation de la gauche (2023-12-10)

.. _leftrenewal_fr_2023_12_10:

=================================================================================================================================================
2023-12-10 leftrenewal **Pour une gauche démocratique et internationaliste, Contribution au renouveau et à la transformation de la gauche**
=================================================================================================================================================

- https://leftrenewal.net/
- https://leftrenewal.net/french-version/


Commentaires
==============

Commentaires 2024
------------------------

- :ref:`raar_2024:marliere_2024_02_09`


Pour une gauche démocratique et internationaliste, Contribution au renouveau et à la transformation de la gauche
===================================================================================================================

C’est avec horreur que nous voyons augmenter depuis des semaines, jour après
jour, le nombre de civils tués à Gaza. Nous sommes consternés et indignés
par la punition collective infligée aux Gazaouies par l’armée israélienne,
par la violence grandissante des colons en Cisjordanie et par l’oppression
des citoyens palestiniens d’Israël par le gouvernement et les groupes
d’extrême-droite. Aux USA, en Europe, en Inde et ailleurs, l’activisme en
faveur des Palestiniens est diabolisé par de nombreux politiciens, par un grand
nombre de médias et criminalisé par l’État dans certains cas. Beaucoup
d’analyses occidentales sur le conflit israélo-palestinien sont imprégnées
de racisme et d’esprit colonial, présentant souvent les Israéliens comme un
peuple occidental moderne et civilisé dont les souffrances sont plus profondes
et avérées que celles endurées par les Palestiniens.

Dans un tel contexte, il est compréhensible de se focaliser exclusivement sur
le court-terme. On pourrait penser que ce n’est pas le moment de parler de
ce qui pèche dans l’activisme relatif au conflit Israël/Palestine et plus
généralement au sein des gauches.

Mais nous pensons que face à cette crise, la réflexion est d’une importance
capitale. C’est maintenant, et non plus tard, que nous devons faire preuve
d’esprit critique afin de décider si les objectifs principaux de nos
mouvements sont les bons pour réellement obtenir un changement.

Quantité de ce que l’on dit et de ce à quoi on croit à gauche au sujet
de l’impérialisme et de l’anti-impérialisme, du nationalisme et de
l’internationalisme, du racisme, du djihadisme et de beaucoup d’autres
sujets est, à nos yeux, profondément biaisé et parfois réactionnaire.

Trop de militants des gauches ont défendu ou même célébré le massacre du 7
octobre perpétré par le Hamas et le Jihad islamique dans le sud d’Israël. De
notre point de vue, c’est là l’expression de ces analyses biaisées et
de ces opinions réactionnaires.

Nous sommes depuis longtemps des militant.es et organisateur.ices des
gauches. Avec ce texte, nous voulons initier un dialogue avec les différents
courants des gauches, et faire comprendre à celles et ceux qui souhaitent
faire de même qu’ils et elles ne sont pas seul.es. C’est également une
invitation à nous rejoindre pour nous dresser contre l’antisémitisme,
l’antiracisme tronqué, le campisme, le nationalisme, la tolérance envers
le djihadisme et les autres alliances confusionnistes gauche-droite. Nous
écrivons avec l’espoir qu’un autre internationalisme est possible.

Le but de notre critique n’est pas de modérer le soutien de la gauche aux
droits et à la liberté des Palestiniens, mais bien de ré-ancrer ce soutien
dans un projet démocratique viable, enraciné dans l’internationalisme, et
donc vraiment universaliste. Nous voulons une gauche qui lutte plus efficacement
non seulement pour les droits des Palestiniens, mais aussi pour la démocratie,
l’égalité et la liberté pour toutes et tous.

Alors que de nombreuses images, qui nous sont initialement parvenues le 7
octobre depuis la frontière de Gaza, montraient des civils franchissant
les clôtures, dès le milieu de la matinée, il était clair que le Hamas
et ses alliés avaient impitoyablement assassiné quantités de civils sans
armes et kidnappé d’autres. Les victimes étaient jeunes comme vieilles,
comprenaient des survivants de la Shoah, des travailleurs agricoles migrants et
des Bédouins. On possède les preuves indubitables de torture et de violences
sexuelles d’une extrême gravité. L’ampleur et la barbarie des attaques
ont fait résonner la peur et le trauma non seulement au sein de la société
israélienne mais aussi à travers la diaspora juive du monde entier puisque
la plupart des Juifs – sionistes ou non – ont de multiples liens avec
Israël. Les massacres du 7 octobre, et les attaques à la roquette sur les
civils israéliens, sont des actes dictés par la cruauté la plus pure qui
causent une profonde souffrance aux Juifs d’Israël et de la diaspora.

Mais l’apologie de la violence du Hamas envers les civil.es portée par
une bonne partie de l’extrême-gauche révèle non seulement un manque de
compassion la plus élémentaire, mais également une appréciation erronée
de ce qu’est le Hamas. Le Hamas n’est pas une simple expression abstraite
de la "résistance" à Israël. Il effectue ses actions selon ses propres
objectifs politiques, lesquels sont fondamentalement réactionnaires. Éluder
ces objectifs au profit d’un soutien inconditionnel à toute "résistance
» revient à dénier aux Palestiniens leur libre-arbitre et à réduire
ceux-ci à une simple force de réaction incapable d’effectuer des choix
politiques. S’opposer au Hamas n’est pas "expliquer aux Palestiniens
comment résister" mais bien soutenir les Palestiniens qui s’opposent
aussi au Hamas et qui mènent la résistance sur une base politique différente.

Les actions du Hamas ont été suivies d’une réponse massive de l’état
israélien, ainsi que le Hamas l’avait anticipé et espéré. Il faut le
réaffirmer : nous sommes consternés et nous nous opposons catégoriquement aux
attaques de l’État israélien sur les civils et les infrastructures civiles
de Gaza, au déplacement des populations palestiniennes, à la rhétorique
déshumanisante et aux intentions de nettoyage ethnique formulées par les
politiciens israéliens, aux projets d’implantation de colonies à Gaza et
à la violence des colons et des forces de sécurité israéliennes exercée
contre les Palestiniens de Cisjordanie. Nous soutenons la lutte en faveur des
droits des Palestiniens et nous opposons à la violence de l’État israélien
et à l’occupation.

Mais si notre mouvement doit effectivement poursuivre ses objectifs
d’émancipation et de démocratie, il doit y avoir un lieu de réflexion et
de critique des courants qui agissent au sein de la gauche contre ces mêmes
objectifs.

Reconnaître la souffrance des Palestiniens ne veut pas dire que nous ne pouvons
pas également réflechir aux problèmes posés par beaucoup de réactions à
gauche, et plus généralement aux perspectives des gauches suite au 7 octobre.

Dans la foulée des attaques, les actes antisémites – attaques violentes,
harcèlement en ligne et dans la réalité – ont explosé dans le monde. Les
discours antisémites se sont répandus viralement sur les réseaux sociaux et
dans les rues. L’islamophobie a aussi fortement augmenté. L’extrême-droite
instrumentalise ce conflit pour toucher de nouveaux publics, autant parmi
les soutiens que parmi les opposants à Israël. La polarisation croissante
et la division contribuent à la déshumanisation des juifs, des Arabes et
des musulmans partout, et pas seulement des Israéliens et des Palestiniens,
ainsi qu’à la diffusion d’une concurrence victimaire dans un jeu à somme
nulle en lieu et place de la solidarité.

Nous nous opposons aux tentatives visant à discréditer, diaboliser ou même
criminaliser tout mouvement de solidarité avec les Palestiniens au prétexte
de =de l’antisémitisme au sein du mouvement pro-palestinien et de la
gauche en général. Néanmoins, une confrontation avec l’antisémitisme
reste nécessaire.

Il ne s’agit pas d’une question de respectabilité du mouvement
pro-palestinien. La raison pour laquelle il faut se confronter à
l’antisémitisme lorsqu’il apparaît n’est pas qu’il rend suspecte
la solidarité avec les Palestiniens, mais que la présence de positions
réactionnaires et conspirationnistes dans nos mouvements, même sous forme
codée ou marginale, risque d’intoxiquer nos projets politiques.

Où la gauche s’est-elle perdue ?
==================================

Pourquoi est-il si difficile pour une large part des gauches d’adopter une
attitude humaniste élémentaire et de prendre en compte la souffrance des civils
– y compris des citoyens israéliens – comme point de départ ? Pourquoi
certains ont-ils été incapables de condamner un massacre sans chercher à le
relativiser ou le contextualiser de façon insensée ? Pourquoi la solidarité
des militants des gauches avec les victimes d’oppression semble-t-elle parfois
conditionnée à l’alignement géopolitique de l’État qui les opprime
? Pourquoi un bonne partie des courants de la gauche doit-elle se battre pour
identifier l’antisémitisme et y faire face dans ses propres rangs ?

Il n’y a pas de réponse unique et simple à ces questions mais nous croyons
que commencer à y répondre est une étape essentielle pour un renouveau des
gauches. Nous présentons notre analyse de ce que nous considérons ici comme
certains des problèmes les plus importants.

Fétichisation du conflit Israël/Palestine
================================================

Le conflit Israël/Palestine est devenu un drame moral central pour une large
part de la gauche contemporaine, de la même manière que le fut l’Afrique
du Sud pour la génération précédente.

Certains reportages et commentaires journalistiques grand public utilisent
un cadre intellectuel orientaliste pour décrire toute la région, faisant
le portrait d’Arabes barbares et pré-modernes, à l’opposé d’Israël
habituellement représenté comme une démocratie libérale moderne.

Par ailleurs, les organes de presse dominants, tout comme la presse engagée
à gauche, accordent beaucoup plus d’attention au conflit Israël/Palestine
qu’à la Syrie, au Kurdistan, au Soudan, à l’Éthiopie, à la RDC,
au Sri-Lanka, au Myanmar ou à tout autre endroit du monde où des états
militaristes (ou des acteurs non-étatiques) oppriment des minorités nationales
et ethniques ou se livrent à des massacres.

Il ne s’agit pas d’établir une hiérarchie morale ou politique
des oppressions dans le monde ni de prioriser l’attention et l’action
en fonction du degré de souffrance. Il s’agit plutôt de montrer que la
solidarité avec les Palestiniens doit trouver sa source dans un engagement
universel en faveur des droits fondamentaux, ce qui suppose également la
solidarité avec toutes les autres luttes contre l’oppression.

En fétichisant le conflit Israël/Palestine et en idéalisant la lutte des
Palestiniens dans une imagerie romantique, les partisans des gauches = se font
le miroir de la déshumanisation des Palestiniens régnant dans l’opinion
générale. L’effet de cette fétichisation du conflit Israël/Palestine
par les gauches est de faire des Palestiniens comme des Juifs israéliens les
personnages abstraits d’un récit politique plutôt que des êtres humains
de chair et de sang, susceptibles de réagir de diverses manières à leur
condition et leurs expériences.

Ignorance de l’Histoire
==========================

En dépit de la centralité de la cause palestinienne dans la gauche
contemporaine, il y règne souvent une méconnaissance de l’histoire de la
région et du conflit.

Une grande partie des militants des gauches ont détourné le sens de
concepts potentiellement utiles à l’analyse, tels que le "colonialisme
de peuplement" pour les transformer en pseudo-analyses. User de ces termes
de façon simpliste permet aux militants d’éviter la confrontation avec
la complexité. La diversité historique interne du sionisme, sa relation
ambivalente aux divers impérialismes et les histoires variées des exils qui
ont donné lieu aux migrations juives vers Israël depuis un certain nombre
de pays sont souvent mal comprises.

Le processus de formation d’Israël comme foyer national juif impliquait une
colonisation de peuplement entraînant le déplacement de nombreux habitants
initialement présents, au prix de crimes de guerre et d’expulsions. Ce fut
aussi la fuite éperdue d’un peuple qui avait lui-même été victime de
la violence raciste et d’une tentative d’extermination. Les Palestiniens
sont, d’après une phrase d’Edward Saïd, "les victimes des victimes et
les réfugiés des réfugiés". Les Juifs israéliens sont loin d’être
les seuls à s’être constitués en nation et à avoir fondé un État sur
des bases qui entraînaient la dépossession des habitants déjà présents
de leur propre territoire.

Se confronter entièrement à cette histoire, avec toute sa complexité et ses
tensions, n’est pas destiné à minimiser la souffrance endurée par les
Palestiniens lors de la fondation d’Israël et après. Mais manquer de le
faire ruinerait la compréhension et les efforts déployés pour développer
et soutenir les luttes pour l’égalité.

Un meilleur niveau de connaissance historique et une prise en compte plus
engagée des implications pratiques des différentes propositions de «
solutions" possibles, solution à un État, à deux États, ou autres,,
permettrait de revivifier le mouvement de solidarité.

Politique confusionniste
===========================

Une clé d’analyse importante de la politique contemporaine, dans le sillage de
l’effondrement des mouvements ouvriers de masse, est la montée en puissance
des projets politiques syncrétiques qui se dessinent sur des traditions
politiques disparates – qu’on appelle parfois la politique rouge/brune,
le diagonalisme ou le confusionnisme. Certaines franges de la gauche ont
conclu des alliances dangereuses avec des forces d’extrême-droite. Des
tribuns d’extrême-droite présents dans les manifestations contre la guerre
aux anciens militants de gauches rejoignant la contestation du confinement
résultant de la Covid, des influenceurs anti-impérialistes recevant des
invités paléo-conservateurs aux chanteurs de folk anarchistes promouvant des
négationnistes, ces derniers temps ont vu naître des collaborations politiques
alarmantes. Parfois, ces mouvements naissent ou se développent à partir de
franges de l’extrême-droite essayant de se vendre à la gauche. Puisque
l’antisémitisme est souvent le ciment des éléments disparates dans ces
formations syncrétiques, ces courants peuvent se révéler toxiques quand
ils se manifestent dans le militantisme en solidarité avec la Palestine.

**Campisme**
==============

- http://cntaittoulouse.lautre.net/spip.php?article1372 (2023-12-09 En finir avec le "campisme", ajout NDLR)

Dans le monde entier ont lieu des luttes pour le changement démocratique
et pour gagner plus de droits et d’égalité. Mais elles sont de plus en
plus souvent associées aux revendications selon lesquelles ces principes
représentent l’hégémonie de "l’élite libérale occidentale" et
son "ordre mondial unipolaire" plutôt que les droits et les aspirations
humaines universels.

Les régimes oppresseurs et autoritaires affirment que les efforts fournis pour
les rendre redevables de ces principes ne sont que des tentatives de protéger
l’hégémonie unipolaire occidentale. Ces régimes se présentent comme les
leaders d’un monde "multipolaire" émergent, où les différents régimes
autoritaires seront libres de définir la "démocratie" selon leur propres
standards antidémocratiques.

Par ailleurs, de même que les mouvements racistes, patriarcaux et autoritaristes
occidentaux se proclament la voix des peuples de souche authentiques contre les
"élites mondialistes", ces régimes se présentent comme la "majorité
décoloniale" contre l’hégémonie des "élites occidentalisées" dans
les anciennes colonies occidentales.


Les gauches refusent souvent de reconnaître cette dynamique. Pire, certaines de
ses composantes diffusent une (fausse) promesse : celle qui veut que des régimes
et des forces tyranniques, autoritaires et réactionnaires représentent une
résistance progressiste à l’"Occident impérialiste". Leur engagement
pour la survie et la solidité de ces régimes "multipolaires" se fait
au détriment d’une solidarité consistante, efficace et significative avec
les mouvements de résistance à ces régimes.

L’impérialisme occidental est confronté au défi des alternatives
réactionnaires : l’impérialisme russe, l’impérialisme chinois,
l’impérialisme régional iranien, déploient souvent des forces paramilitaires
écran telles que le Hezbollah et, dans une certaine mesure, le Hamas,
et jouent un rôle contre-révolutionnaire, notamment durant la vague des
mouvements de libération qui s’est élevée en 2011. Les pétro-monarchies
de la péninsule arabe deviennent progressivement des puissances globales ;
d’autres puissances impérialistes régionales ou sub-impérialistes telles
que la Turquie deviennent de plus en plus fortes et ne sont certainement pas
de simples États clients des USA.

Face à cette situation, une gauche radicale qui prêche depuis des années que
tout ce qui nuit à l’impérialisme hégémonique occidental (celui des USA)
et à ses alliés est nécessairement progressiste (une perspective connue comme
le "campisme" – s’associant à un "camp" géopolitique plutôt que
poursuivant un projet authentiquement internationaliste) ne peut que s’abîmer
dans l’apologie de ces régimes réactionnaires. Cet "anti-impérialisme
» campiste est aveugle au fait qu’en soutenant le soi-disant "axe de
résistance", il ne s’oppose nullement à l’impérialisme mais est
l’auxiliaire d’un pôle impérial rival dans un monde "multipolaire".

Dans une période historique antérieure (atteignant son point culminant durant
la Guerre Froide), le pôle d’opposition aux USA dans l’imaginaire de la
gauche campiste était l’URSS (qui était plus un frein a une quelconque
alternative plutôt qu’une réelle boussole politique). Mais après le choc
pétrolier de 1973 et la révolution iranienne de 1979, et particulièrement
après la chute du bloc soviétique, ce rôle fut progressivement assuré par
diverses configurations de l’"axe de résistance" incluant la République
islamique d’Iran et peu après le Hamas.

Théorie du complot
========================

Notre monde complexe et "multipolaire", l’opacité des mécanismes de
pouvoir et d’oppression ainsi que les processus de fragmentation sociale
conduisent les gens à rechercher des réponses à leurs questions et des
explications au-delà de "la pensée dominante". Les plateformes numériques
qui monétisent la désinformation et les fausses informations, qui facilitent
la propagation des mythes et des mensonges, facilitent l’accès aux théories
conspirationnistes qui donnent l’impression d’apporter des réponses et des
explications. Les façons de partager et d’acquérir la connaissance par le
tout numérique, encouragent simultanément le cynisme envers les institutions
et la crédulité vis-à-vis des sources "alternatives". La joie de «
démasquer" les réalités cachées conjointement au désespoir devant
l’omnipotence de l’hégémon et la recherche de liens de causalité entre
des phénomènes disparates en l’absence des outils analytiques permettant de
comprendre le sens de ces derniers. Et les théories complotistes conduisent
presque invariablement à l’antisémitisme, qui fonctionne comme une
méta-théorie de la conspiration.

L’antisémitisme fusionne souvent avec le fanatisme anti-musulman dans
l’imaginaire de l’extrême-droite conspirationniste, par le biais des
théories du "Grand Remplacement" qui postulent un complot fomenté par des
"financiers mondialistes", dont le plus éminent est Georges Soros, afin
de sponsoriser l’immigration musulmane en Europe, aux USA et dans d’autres
contrées à majorité "blanche" et déplacer les populations "blanches".

L’antisémitisme comme pseudo-émancipation
================================================

Comme d’autres théories complotistes, l’antisémitisme offre de fausses
réponses et des explications faciles dans un monde complexe et confus. A
l’inverse de beaucoup d’autres racismes, l’antisémitisme apparaît
souvent comme un "coup de poing" : il peut attribuer à l’objet de
sa haine un pouvoir, une richesse et une ruse presque infinis. En raison
de son caractère pseudo-émancipateur, l’antisémitisme paraît souvent
radical. Mais c’est un pseudo-radicalisme : en assimilant les Juifs à une
élite cachée contrôlant nos sociétés, il sert à rendre la vraie classe
dominante invisible, à en protéger les structures de pouvoir et à détourner
sur les Juifs la colère face à l’injustice.

Ainsi que Moishe Postone le suggérait, il agit souvent comme une forme d’«
anti-capitalisme fétichisé" : "La puissance mystérieuse du capital,
qui est intangible, globale et qui emporte les nations, les régions et
les vies des gens, est attribuée aux Juifs. La domination abstraite
du capitalisme est personnifiée par les Juifs". Cet antisémitisme
pseudo-émancipateur a une longue histoire, s’étalant depuis certains
textes fondateurs de branches maîtresses du socialisme moderne jusqu’aux
congrès de la Seconde Internationale, aux syndicats et partis ouvriers au
temps des migrations de masse d’Europe de l’Est, aux versions New-age
du fascisme dans le mouvement écologiste. Il était présent, et contesté,
au sein des partis de la Révolution russe, s’exprimait dans le nazisme et
l’idéologie stalinienne d’après-guerre et est repris par les héritiers
d’aujourd’hui, qui voient chez des "financiers mondialistes" et «
cosmopolites" une pieuvre vampire exploitant les travailleurs productifs
enracinés dans leur terre de naissance. Mais cet antisémitisme est aussi
de plus en plus souvent jumelé avec une vision "anti-impérialiste" dans
laquelle on suce le sang des miséreux du Sud Global.

Accointances avec le djihadisme
====================================

Bien que certains pans de la gauche (particulièrement en Europe et dans les
Amériques, mais également dans d’autres régions du monde) possèdent une
longue histoire d’islamophobie (revenu à l’avant-plan pendant la guerre
syrienne, alors que des blocs de gauche usaient du vocable de guerre contre
le terrorisme pour diaboliser la révolution), dans la période qui suivit la
Seconde Intifada et le 11 septembre, la conception du monde campiste décrite
plus haut a conduit beaucoup de gens de gauche à considérer le djihadisme comme
une force progressiste ou même révolutionnaire en regard de l’impérialisme
occidental hégémonique.

C’est, malheureusement, un phénomène mondial. Cependant, la plupart des
militants de gauche d’Asie du sud-ouest et d’Afrique du Nord (SWANA),
qui ont été confrontés plus directement à la politique réactionnaire
des djihadistes que dans les autres régions du monde, n’entretiennent pas
de telles illusions, bien au contraire. Les partisans de la gauche radicale
vivant en dehors de la SWANA feraient bien de les écouter.

Le djihadisme englobe différentes branches. Le Hamas n’est pas Daesh, Daesh
n’est pas les Talibans, les Talibans ne sont pas le régime d’Erdoğan en
Turquie. Le Hamas lui-même comprend différents courants. Il est important
de comprendre ces distinctions. Mais cela ne doit pas aveugler les gauches
sur le fait qu’au niveau social du pouvoir, les mouvements et les régimes
djihadistes, tout comme d’autres formes de fondamentalisme religieux politique,
maltraitent les minorités religieuses, ethniques et sexuelles, les femmes,
les dissidents politiques et les mouvements progressistes.

Le racisme anti-juif est un élément persistant de l’idéologie djihadiste,
clairement affiché dans le travail fondateur de Sayyid Qutb "Notre combat
contre les Juifs" (1950), et dans la Convention de 1988 du Hamas (qui cite
le faux document anti-juif des Protocoles des Sages de Sion). Les positions
des djihadistes sur Israël, sur le sionisme et sur les Juifs ne sont pas
purement "politiques", c’est-à-dire explicables seulement en termes de
confrontation entre Palestiniens et Israéliens/sionistes, mais participent
plus largement d’une vue antisémite du monde.


Bien qu’ils aient leurs propres perspectives et agendas, les mouvements
djihadistes doivent être compris dans le contexte de la compétition entre
puissances régionales dans un monde d’impérialismes concurrents : les
djihadistes s’opposent souvent à l’impérialisme hégémonique au nom
d’un impérialisme régional rival ou en s’alliant à celui-ci – tel
que celui d’Iran. Dans le même temps, l’impérialisme américain et ses
alliés, comme Israël, tolèrent ou appuient parfois également les mouvements
djihadistes afin d’affaiblir d’autres forces.


La façon de voir les luttes de libération relatives au genre et à la
sexualité comme étant d’importance politique secondaire par rapport à
d’autres problèmes tels que, par exemple, la lutte contre "l’ennemi
commun" qui est "l’impérialisme des USA", permet aussi en partie
d’expliquer la volonté manifestée par beaucoup de militants de la gauche
radicale de blanchir, faire taire les critiques sur ou même proposer des
alliances avec des mouvements qui, comme tous les mouvements fondamentalistes
religieux, sont obsédés par le contrôle patriarcal, homophobe et transphobe
du genre et de la sexualité.

Abandon de l’analyse de classe
===================================

La seule voie possible pour déboucher sur un véritable projet politique
démocratique et anticapitaliste est le soutien aux luttes auto-organisées des
exploités et des opprimés qui vont dans le sens de l’émancipation. La lutte
des classes n’a cessé de reculer depuis des décennies suite aux victoires
du néolibéralisme et aux défaites du mouvement social et ouvrier. Mais
l’abandon du soutien à l’auto-organisation de la classe ouvrière et des
autres luttes d’émancipation contribuant à la démocratie à partir de
la base relève d’une longue histoire. Le siècle dernier a vu quantité
d’instances des gauches substituer la volonté des états staliniens et
d’autres forces autoritaristes à celle des exploités et des opprimés.

Beaucoup de gens qui se définissent eux-mêmes comme étant de gauche ont été
jusqu’à soutenir, avec un point de vue plus ou moins critique, les forces
étatiques et non-étatiques qui ne se réclament même pas de la rhétorique
ni du symbolisme du socialisme : la Russie de Poutine, la Syrie d’Assad,
la République islamique d’Iran et les forces paramilitaires telles que le
Hamas et le Hezbollah.

Nous pensons que la montée des projets politiques confusionnistes, campistes
ou complotistes, ainsi que le ralliement croissant à l’antisémitisme
pseudo-émancipateur, peuvent s’expliquer partiellement comme une conséquence
de cet abandon du concept de classe par la gauche et de la dynamique globale
du capitalisme.

Une grande partie des politiques à gauche de ces dernières décennies ont
été fondées non pas tant sur la lutte contre le capitalisme en tant que
relation sociale, mais bien comme le rejet de l’"hégémonie américaine",
de la "mondialisation", de la "finance" – ou parfois du "sionisme"
considéré comme l’avant-garde de toutes ces forces. Cela a conduit beaucoup
de gens qui se vivent comme étant de gauche à avoir de la sympathie pour
les alternatives réactionnaires à l’ordre politique et économique actuel.

Dans le même temps, des versions tronquées de l’anti-capitalisme qui se
concentrent sur l’immoralité supposée du capital "financier" ou «
improductif" – plutôt que sur l’antagonisme objectif entre le capital
et le travail – encouragent les critiques personnalisées des "élites
mondialisées" et des "banquiers de Rothschild" au lieu d’un mouvement
en faveur de l’abolition du capitalisme lui-même grâce à l’organisation
collective et à la lutte provenant de la base.

Antiracisme sélectif
==========================

L’antiracisme global contemporain a été modelé dans le contexte d’un 20e
siècle dominé par les luttes contre le racisme anti-noirs, aux USA et ailleurs,
et contre l’impérialisme occidental et le colonialisme. Son appréhension de
la notion de race est souvent simpliste, binaire et mal conçue pour comprendre
les lignes d’intersections de la racialisation du 21e siècle.

La perspective dominante d’une bonne part de la pensée "décoloniale"
présente une vision manichéenne qui divise le monde en "oppresseurs" et
"oppressés", deux catégories que cette pensée applique à des nations
et des peuples dans leur entièreté.

Cette vision handicape la gauche lorsqu’elle doit comprendre comment les
différents racismes se répondent – pourquoi les suprémacistes hindous
d’Inde appuient le nationalisme israélien avec enthousiasme, par exemple,
ou pourquoi l’État chinois suprémaciste Han se présente comme l’avocat
des droits des Palestiniens tout en se livrant à une colonisation et à une
répression de masse des musulmans dans le Xinjiang/Turkestan de l’est au
nom de la "Guerre du peuple contre le terrorisme".

Et elle handicape aussi la gauche lorsqu’elle doit comprendre le racisme
quand il ne correspond pas à une question de couleur de peau, comme pour
le racisme des Européens de l’ouest contre les Européens de l’est «
pas si blancs que ça", le racisme des Russes contre les Ukrainiens ou le
racisme anti-Arméniens.

L’antisémitisme en particulier ne rentre pas clairement dans la vision du
monde de cet antiracisme sélectif, qui voit les Juifs comme "blancs" et
ne peut par conséquent pas les considérer comme victimes de racisme. Cette
perspective efface les Juifs qui ne se présentent pas comme "blancs" et
passe à côté de la construction sociale et contingente de la blanchité
elle-même. L’intégration de certains Juifs dans la blanchité est réelle
mais elle est inégale et très récente dans de nombreux cas.

[Note de la traduction : cette partie relève de la conception anglo-saxonne du
terme de blanchité. Pour notre part la blanchité n’a rien a voir avec la
couleur de peau puisqu’elle est un rapport social. Dans ce sens, les juifs
ne sont pas "blancs" mais tout simplement "juifs" puisqu’ils sont
désignés comme appartenant à la supposée "race juive" à partir du XIXe]

Cet antiracisme tronqué est le reflet de l’anticapitalisme tronqué dans lequel se perdent les gauches
==========================================================================================================

En résumé, le renouveau de la gauche en tant que mouvement pour la solidarité
internationale nécessite un antiracisme et un féminisme consistant, une
régénérescence de la lutte des classes, un renouveau de l’analyse du
capitalisme au niveau mondial et le rejet d’une vision campiste qui procède
d’une division binaire du monde en catégories du bien et du mal.

Comment pouvons-nous transformer et régénérer la gauche ?
==============================================================

Nous proposons cette analyse comme une contribution pour un renouveau des
gauches fondé sur un projet politique authentiquement internationaliste
et démocratique. Il n’est pas toujours aisé de s’attaquer aux idées
réactionnaires dans nos propres rangs. Mais quand nous le faisons, notre
mouvement s’enrichit, à chaque fois, de la compréhension plus profonde qui
s’en dégage. À quoi cela ressemblerait-il si les gauches prenaient cette
analyse en compte ?

Une solidarité consistante
==================================

En tant qu’internationalistes, notre point de départ devrait être
la promotion du droit universel à l’accession des peuples aux droits
démocratiques. Insister sur la solidarité avec les civils attaqués des
deux côtés n’est pas un style désinvolte d’équivalence morale ou
d’argutie mais bien un principe éthique fondamental. La vraie solidarité
ne veut pas dire qu’il faut voir toutes les personnes comme étant les
mêmes et n’ignore pas les différences structurelles entre les victimes ;
au contraire, elle reconnaît et respecte les différentiations.

Les gauches devraient se préoccuper des morts civiles, qu’elles soient
causées par l’État juif ou par les États arabes, par les États du camp
occidental ou par les États qui s’y opposent ou encore par des acteurs
non-étatiques.

La fin est substantiellement conditionnée et préfigurée par les moyens :
une politique menée au moyen de massacres indiscriminés de civils ne sert
pas une fin émancipatrice.

Sont particulièrement problématiques les courants politiques qui mettent
la lumière sur les souffrances des Palestiniens à Gaza tout en restant
silencieux – si ce n’est enthousiastes – lorsque les Syriens (y compris
des Syriens palestiniens) ont été massacrés par le gouvernement d’Assad et
ses alliés (massacres souvent justifiés par exactement la même rhétorique
de la guerre contre le terrorisme que celle dont use Israël pour excuser
les frappes contre les civils) ou que les Ouïghours et d’autres minorités
ethniques majoritairement musulmanes font face aux incarcérations de masse,
à la surveillance totale et à l’anéantissement culturel en Chine.

Donner de la voix aux forces militantes des travailleurs et des travailleuses,
des progressistes, et de ceux et celles qui agissent pour la paix des deux
côtés.

Le changement démocratique radical est impossible sans la volonté propre de se
battre consciemment et activement pour celui-ci. Une gauche internationale qui
consacre ses énergies à faire le jeu des forces réactionnaires ne fait rien
pour aider au développement de cette volonté ; au contraire, elle l’inhibe.

Dans le conflit Israël/Palestine, comme dans toute lutte internationale, une
gauche véritablement internationaliste et démocratique devrait concentrer son
activité sur l’écoute, l’engagement et la constitution d’un soutien
concret aux forces présentes sur le terrain qui s’organisent pour faire
progresser un agenda démocratique. Cela signifie offrir une caisse de résonance
à la voix des acteurs locaux – les féministes, les militant.es queers,
les syndicalistes, les activistes de l’environnement – qui s’opposent
dans les sociétés israélienne et palestinienne à la violence d’État
perpétuelle et à la division raciste.


Critiquer les États ne veut pas dire s’opposer aux droits fondamentaux des peuples
====================================================================================

Les communautés nationales dans leur ensemble bénéficient souvent des
politiques colonialistes de leurs états et de l’oppression des autres
peuples. Mais ces bénéfices ne sont pas uniformes et ne cela ne veut pas
dire que tous leurs membres sont également complices ou qu’ils ont le même
pouvoir sur les politiques de leur État.

La solidarité avec les Palestiniens ne devrait pas être synonyme d’hostilité
monobloc envers les Juifs israéliens en tant que peuple ni s’opposer aux
droits fondamentaux de ces derniers. Un projet politique de gauche doit viser
à aligner les droits démocratiques de tous sur le mieux-disant et non à
les soustraire aux uns pour les "redistribuer" à d’autres.

Partout, les Juifs – qui sont souvent liés à des personnes ou des lieux
en Israël – se sentent attaqués quand les Israéliens en général sont
ciblés. Soutenir les droits des Palestiniens demande d’identifier et de
nommer précisément l’État d’Israël et son appareil idéologique comme
les auteurs de l’injustice, et non le peuple israélien comme un tout,
considéré comme un bloc homogène et indifférencié sur le plan politique.

Comprendre Israël dans le monde
=====================================

L’antisémitisme attribue traditionnellement un pouvoir absolu aux
Juifs. Quand ce schéma s’applique à Israël, c’est toujours de
l’antisémitisme. Israël existe dans un monde complexe, mouvant, «
multipolaire" ; c’est un État puissant mais son pouvoir est limité dans
le système mondial. Il n’est certainement pas le leader de l’impérialisme
mondial que l’on dépeint dans certains narratifs à gauche.

Il y a un grand nombre de raisons pour lesquelles il est juste et nécessaire
de critiquer Israël, et qui sont les mêmes raisons de critiquer par bien des
États dans le monde, y compris certains des pays dans lesquels nous vivons
nous-mêmes. Refuser de diaboliser Israël ou le considérer comme exceptionnel
ne veut pas dire être d’accord avec sa politique, mais plutôt inscrire
cette politique dans les tendances dont elle est l’une des expressions et
non la quintessence. Même la violence à grande échelle qu’Israël inflige
aux habitants de Gaza trouve un précédent récent dans la guerre du régime
de Assad contre le peuple syrien.

Les courants des gauches qui critiquent le colonialisme d’implantation
israélien tout en faisant l’apologie du colonialisme russe en Ukraine
utilisent un double-standard. Nous pressons aussi nos camarades de réfléchir
pour savoir si eux ou leurs organisations utilisent le même langage ou le
même registre émotionnel au sujet de l’oppression des Kurdes par la Turquie
ou des Tamouls par le Sri-Lanka, par exemple, qu’au sujet de l’oppression
des Palestiniens par Israël. Si la réponse est non, considérez l’impact
politique et les implications de cette exceptionnalisme.


Une approche critique du nationalisme
========================================

Les nations sont des constructions sociales qui fonctionnent en partie pour
masquer les exploitations et les oppressions en leur sein, telles que celles
liées à la classe sociale, au genre, à la race, ou autre, au nom d’un «
intérêt national" uniforme. Notre objectif de long terme est une association
libre de tous les êtres humains, c’est-à-dire un monde sans nations, dans
lequel les distinctions ethniques deviennent secondaires. Cependant, transcender
la nationalité est difficile à concevoir dans un monde où les peuples sont
oppressés, occupés et parfois massacrés en raison de leur histoire nationale.

Les gens de gauche doivent se dresser contre l’oppression des peuples liée à
leur nationalité. Mais nous devons aussi reconnaître que tous les nationalismes
– y compris ceux des groupes opprimés actuellement – sont, au moins
potentiellement, vecteurs d’exclusion et d’oppression. Soutenir le droit
de se défendre d’un peuple donné ou de conquérir l’autodétermination
ne signifie pas pour autant embrasser leur nationalisme par procuration. Une
gauche internationaliste ne doit pas brandir un drapeau national ou soutenir
un État ou un mouvement national de façon indistincte.

La gauche doit soutenir le droit à l’autodétermination comme faisant partie
d’un programme pour l’égalité démocratique. Cela suppose de promouvoir
sur une même base le droit de tous les peuples à l’autodétermination et
de s’opposer à tout programme qui ambitionne la domination d’un peuple
sur un autre.

L’objectif du Hamas de remplacer la domination nationaliste juive par
une domination nationaliste islamiste – un état théocratique dont les
"usurpateurs" juifs seraient expulsés – est réactionnaire. Le fait
qu’il soit hautement improbable qu’ils parviennent à leurs fins ne rend
pas leur objectif plus supportable du point de vue d’une ambition politique
démocratique internationaliste.

Antiracisme inconditionnel
=================================

Les raisons pour lesquelles il faut se tenir aux côtés des victimes du
racisme ne se limitent pas à la compassion pour la sensibilité blessée
des personnes concernées – bien que la préoccupation soit préférable à
l’indifférence parfois affichée à gauche. Elles tiennent aussi à ce que
les idées qui incitent au sectarisme minent les efforts pour faire progresser
les luttes en faveur de la démocratie.

Cela suppose de refuser de conditionner notre solidarité contre le racisme
à la politique.

Tout comme c’est un tort de demander aux Palestiniens (ou aux autres Arabes ou
musulmans) de condamner le Hamas avant de leur donner droit au soutien contre
le racisme, c’en est un également de demander aux Israéliens ou aux Juifs
de la diaspora de faire la démonstration de leur pureté idéologique –
montrer qu’ils sont de "bons" Juifs – avant que le sectarisme dirigé
contre eux ne soit pris au sérieux.

La solidarité contre le racisme ne doit pas être conditionnée à
l’approbation de l’opinion politique dominante de la personne ou du groupe
victime de ce racisme. Elle requiert au contraire l’inconditionnalité de
l’opposition au racisme et aux autres sectarismes, même lorsque les membres
du groupe ciblé sont susceptibles d’avoir des idées réactionnaires.

La gauche peut et doit inconditionnellement s’opposer au sectarisme
anti-Palestiniens et anti-musulman sans approuver le Hamas ; elle peut et
doit inconditionnellement s’opposer à l’antisémitisme sans approuver le
chauvinisme israélien.

Ne pas offrir de tribunes aux faux amis
===============================================

Une particularité de la crise actuelle et de ses conséquences est le
cynisme avec lequel les militants d’extrême-droite (à l’inclusion
de purs fascistes et de nazis au sens littéral) instrumentalisent la
solidarité avec les Palestiniens pour répandre l’antisémitisme.
Un petit nombre de ces militants rejoignent les défilés anti-Israël.
Un très grand nombre d’influenceurs pro-palestiniens amplifient l’audience des
contre-influenceurs d’extrême-droite sur les réseaux sociaux, souvent
appuyés par les réseaux d’influence d’État russes et iraniens.
Au cours des semaines qui ont suivi le 7 octobre, des comptes tels que Jackson Hinkle
(un promoteur du "communisme MAGA") et Anastasia Loupis (un militant
antivax d’extrême-droite) ont accumulé des millions d’abonnés parmi
les utilisateurs hostiles à Israël grâce à leurs messages viraux (dont
beaucoup contenaient des fausses informations) au sujet du conflit.

D’autre part, l’extrême-droite n’est pas homogène, et les militants
d’extrême-droite islamophobes, dont beaucoup se sont révélés antisémites
en grattant un peu la surface, instrumentalisent avec cynisme la peur des Juifs
et l’indignation d’un large public face au terrorisme du Hamas pour susciter
l’hostilité anti-musulmane et blanchir leur réputation de racistes.

**Nous devons démasquer et marginaliser ces acteurs répugnants**.
Nous devons tracer des lignes claires.

Nous ne pouvons pas permettre l’instrumentalisation de la souffrance des
Juifs et des Palestiniens par des entrepreneurs politiques.
Tout groupe offrant une tribune aux nazis, aux fascistes et aux orateurs
qui leur sont assimilés doit être traité de la même manière que les
sympathisants des suprémacistes blancs.

Conclusion
================

Nous avons écrit ce texte comme une critique des lignes qui se sont imposées
dans une bonne partie des gauches.
C’est une critique depuis la gauche, et non de la gauche.

En tant que militants des gauches et organisateurs ou organisatrices du mouvement
social, nous ne considérons pas les tendances que nous décrivons comme des
conséquences inévitables des principes fondateurs de la gauche. Nous les
voyons comme le résultat de la déformation et de l’abandon de ces principes.

Les co-signatures additionnelles, y compris de celles et ceux qui approuvent
certaines parties du texte mais pas d’autres, ainsi que les réponses
critiques, sont les bienvenues.
Étant donné le contexte, nous accueillons avec plaisir tout particulièrement
les réponses, y compris critiques, des Palestiniens et Israéliens de gauche.
Nous espérons que ce texte contribue à un large débat sur la transformation
et le renouveau des gauches.

Nous voyons cet effort de régénérescence et de transformation comme une tâche
nécessaire pour toute personne qui ne souhaite pas exclure la possibilité
d’un changement systémique. Nous accueillons favorablement l’engagement de
toute personne investie dans un tel changement et qui comprend que pour être un
instrument efficace pour y parvenir, les gauches doivent se changer elle-même.



10 décembre 2023



Traducteurs : Michaël Goldberg et Jonas Pardo


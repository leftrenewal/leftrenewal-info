.. index::
   pair: Des pensées décoloniales à l’épreuve de la guerre en Ukraine ; Pierre Madelin (2023-02-27)

.. _madelin_2023_02_27:

===============================================================================================
2023-02-27 **Des pensées décoloniales à l’épreuve de la guerre en Ukraine** par Pierre Madelin
===============================================================================================

- https://leftrenewal.org/fr/articles-fr/des-pensees-decoloniales/


Poème par Abdellatif Laâbi
====================================

::

    "Vérité criante
    On peut avancer
    toutes les théories du monde
    sur les dessous de cette guerre-ci
    rappeler tous les crimes commis
    dans le passé
    proche ou lointain
    par les génocidaires
    les esclavagistes
    les colonialistes
    contre l’ensemble des peuples de la terre
    mais on ne pourra pas nier
    la vérité simple
    criante
    irrécusable
    que dans la guerre
    qui nous occupe aujourd’hui
    les Ukrainiens défendent leur terre
    leur liberté
    et les soldats russes
    agissent
    en esclaves aveugles
    d’un tyran"

Abdellatif Laâbi


Introduction
===============

Le 24 février 2022, l’armée russe envahissait l’Ukraine dans le cadre d’une opération
militaire de grande ampleur dont le but était de décapiter rapidement le pouvoir
ukrainien et de soumettre le pays.

Cette invasion brutale, rapidement accompagnée par des crimes de guerre et des
crimes contre l’humanité massifs – bombardements intensifs des infrastructures
et des populations civiles, urbicides comme à Marioupol, massacres comme à Boutcha,
usage fréquent et parfois même systématique du viol et de la torture – a plongé
les gauches mondiales dans un abîme de perplexité.

"Des militants d’habitude si résolus
dans leur soutien de toutes les victimes de la guerre et du capitalisme sont
soudainement devenus extrêmement nuancés et ‘réflexifs’" :ref:`1 <1_2023_02_27>`, ironisait
alors le politologue ukrainien Denys Gorbach dans Lundimatin.

De fait, une frange importante de la gauche, de l’Amérique latine à l’Inde
en passant par la France, adopta des positions dites "campistes".

"Qu’est-ce que le campisme ?", s’interrogent les philosophes Pierre Dardot
et Christian Laval. "C’est la bêtise politique aux effets les plus sinistres
qui consiste à penser qu’il n’y a qu’un seul Ennemi.

On le définira comme un anti-impérialisme à sens unique. De l’unicité de l’Ennemi découle la conséquence imparable suivante :
ceux qui s’opposent à l’Ennemi ont droit sinon aux bénédictions, du moins
aux excuses, selon le principe que les ennemis de l’Ennemi sont, sinon des amis,
du moins des ‘alliés objectifs’ dans un juste combat." :ref:`2 <2_2023_02_27>`

L’influence du campisme
=============================

En France, cette position bénéficie de relais politiques, médiatiques et
intellectuels influents, qui lui donnent un poids non négligeable auprès
d’un public qui, leur faisant confiance, adopte spontanément leurs analyses :
Jean-Luc Mélenchon et une grande partie de l’appareil de la France Insoumise,
qui ont relayé pendant de nombreuses années des éléments de langage proches
de la propagande du Kremlin :ref:`3 <3_2023_02_27>`, ou les journalistes Pierre Rimbert et Serge Halimi
du Monde Diplomatique, qui se défendent de céder au campisme mais qui en ont
adopté à bien des reprises le lexique dans l’un des médias les plus entendus
de la gauche francophone sur les questions internationales.


.. _campisme_chomsky_2023_02_27:

Chomsky
-----------

Dans le monde, le campisme est présent de longue date dans les analyses de
conjoncture proposées par le linguiste américain Noam Chomsky :ref:`4 <4_2023_02_27>`, sans doute l’une
des figures les plus influentes de la gauche au niveau international.

Également très vigoureux au sein de la gauche latino-américaine, il s’est exprimé
dans les positions du président brésilien Lula, affirmant en mai 2022 que Zelensky
est "aussi responsable de la guerre que Poutine" et qu’"il voulait la
guerre.
S’il n’en voulait pas, il aurait négocié un peu plus" :ref:`5 <5_2023_02_27>`,
ou dans celle de l’ancien président bolivien Evo Morales, pour sa part ouvertement
rallié à Poutine, dont il saluait l’anniversaire le 7 octobre 2022 par un tweet
chaleureux : "Toutes nos félicitations au frère Vladimir Poutine pour son
anniversaire. Les peuples dignes, libres et anti-impérialistes accompagnent sa
lutte contre l’interventionnisme armé des USA et de l’OTAN. Les USA doivent
cesser de porter atteinte à la vie." :ref:`6 <6_2023_02_27>`

Tout en condamnant la plupart du temps l’invasion de l’Ukraine et les
exactions de l’armée russe, cette gauche campiste s’est efforcée, usant
d’une rhétorique qui se voulait subversive – non-alignée sur les "médias
dominants" et leur "voluptueux bourrage de crâne" anti-russe :ref:`7 <7_2023_02_27>` – mais dont
les termes semblaient bien souvent sortir tout droit de la propagande du Kremlin,
de minimiser la responsabilité de la Russie si ce n’est dans le déroulement de
la guerre, tout au moins dans son déclenchement.

Anti-impérialistes "coincés dans les coordonnées des années 1960-1970, d’une part,
de la deuxième guerre d’Irak et de la présidence de George Bush Jr, d’autre part" :ref:`8 <8_2023_02_27>`
ou souverainistes hantés par le spectre d’une nation française dépossédée de
son autonomie stratégique par les forces "euro-atlantistes" **s’employèrent
à convaincre leurs auditoires respectifs que la responsabilité en dernière
instance de cette guerre revenait aux États-Unis et à l’OTAN**.

L’"expansion" vers l’Est de l’OTAN après la fin de la Guerre froide,
d’autant plus intolérable qu’elle aurait trahi d’obscures promesses faites
au pouvoir russe dans les années 1990, aurait été menée dans le but d’"encercler"
et d’acculer la Russie, **agresseur ainsi honteusement transformé en victime**.

Ils ne semblaient pas réaliser qu’ils cautionnaient l’idée, profondément anti-démocratique, selon laquelle le monde doit être partagé entre grandes puissances
------------------------------------------------------------------------------------------------------------------------------------------------------------------

Ce faisant, ils ne semblaient pas réaliser qu’ils cautionnaient l’idée, profondément
anti-démocratique, selon laquelle le monde doit être partagé entre grandes
puissances, chacune d’entre elles disposant d’une "sphère d’influence" non
contestée par les autres.

Ni qu’ils tiraient de la factualité des "jeux" géopolitiques la conclusion
que les Etats et les sociétés, en l’occurrence ceux d’Europe de l’Est, pourtant
entrés dans l’Alliance de leur plein gré quelle qu’ait pu être par ailleurs la stratégie
d’influence des Etats-Unis pour les y intégrer, auraient dû se conformer à
ce jeu.

Dès lors, il était en effet naturel à leurs yeux que la Russie, voyant s’échapper
vers l’Europe sa périphérie ukrainienne lui revenant de droit, intervienne
pour la ramener dans sa sphère.

Certains allèrent même plus loin et, épousant un récit aux accents complotistes,
en vinrent à suggérer que les États-Unis avaient tout fait pour provoquer la
guerre afin de briser le rapprochement entre l’Europe et la Russie, obligeant
ainsi le vieux continent à rester, quant à lui, dans la sphère d’influence
américaine. Position où l’on retrouve la trace d’une "vieille grille
de lecture géopolitique, selon laquelle l’Eurasie constitue, compte tenu
de sa taille, de sa démographie et de ses ressources, la clé de la puissance
mondiale. Selon cette vision, les États-Unis savent qu’ils ne font pas le poids
s’ils restent isolés sur leur ‘île’ périphérique. Ils poursuivraient donc
inlassablement un travail de sape visant à fracturer l’unité eurasiatique,
en fomentant des guerres en son sein. La politique américaine consisterait donc
à diviser l’Eurasie, pour mieux régner sur le monde." :ref:`9 <9_2023_02_27>`

La Russie, humiliée, encerclée, provoquée, serait en quelque sorte
tombée dans le piège qui lui était tendu, déclenchant une guerre certes
criminelle, mais néanmoins compréhensible de son point de vue, pour dompter
le "cheval de Troie" de l’expansion impérialiste américaine en Europe :
l’Ukraine.

S’appuyant sur ces postulats discutables et discutés, la gauche campiste, une
fois la guerre déclarée, n’a cessé de présenter celle-ci non comme une guerre
de libération nationale menée par une "petite nation" face à l’agression de
son puissant voisin, mais comme une guerre inter-impérialiste entre la Russie
et les États-Unis, secondés par le gouvernement "aux ordres" de Zelensky.

L’Ukraine, vidée de toute existence et de toute volonté propre, ne serait
finalement que le théâtre sanglant de cet affrontement, et les Ukrainien·nes
ses acteurs·rices et ses victimes le plus souvent inconscient·es.
"Les ventriloques de Washington mènent la danse sur le Vieux Continent",
écrivait ainsi Serge Halimi dans un article au titre évocateur,
"Saigner la Russie" :ref:`10 <10_2023_02_27>`, publié dans le Monde Diplomatique du mois de juin.

Les responsabilités ainsi symétrisées, et **l’Ukraine en tant qu’entité
politique autonome, dotée de sa propre agentivité, ainsi effacée**, une conclusion
fut rapidement tirée : engageons-nous en faveur de la paix et gardons-nous
de donner à Kiev les moyens de sa défense en lui fournissant des armes, car
nous risquerions alors non seulement de "mettre de l’huile sur le feu",
mais aussi de faire le jeu de l’"Empire" en octroyant un avantage décisif
à Washington dans ses ambitions hégémoniques.
Non que la paix ne soit pas pour l’Ukraine comme pour le monde un objectif
désirable, mais face à un agresseur irrédentiste et idéologiquement radicalisé,
ces appels à la paix ont tout d’un voeu pieu, et le refus de soutenir militairement
le camp agressé équivaudrait à le livrer à ses bourreaux.

Obsession anti-américaine, méconnaissance de l’histoire post-soviétique et
déni de l’agentivité des États et des sociétés qui en sont issus font partie
des raisons qui expliquent que cet "anti-impérialisme des imbéciles" :ref:`11 <11_2023_02_27>`,
pour reprendre l’heureuse formule de la Syrienne Leïla Al-Shami, **véritable
naufrage éthique, politique et intellectuel de notre temps**, se soit imposé dans
des pans entiers de la gauche mondiale.

Fort heureusement, et pour s’en tenir au champ intellectuel francophone, de
nombreux·ses auteur·rices issu·es des rangs du libéralisme politique, d’un
certain trotskysme ou des milieux libertaires et autonomes, se sont employés
dès le mois de mars 2022 à dénoncer les faiblesses de ces positions et de
leurs présupposés, offrant en ces temps troublés une précieuse ressource à
tous·tes ceux et celles qu’indignait la rhétorique campiste. :ref:`12 <12_2023_02_27>`


.. _campisme_decolonial:

Un campisme décolonial
============================

Un point n’a cependant peut-être pas été assez souligné par ces différent.es
auteur·ices : la gauche campiste n’a pas été cantonnée à des courants
politiques souverainistes ou issus d’un marxisme suranné focalisé sur la seule
puissance du capitalisme anglo-saxon, **elle s’est également exprimée dans des
médias et chez des penseur·ses associé·es à la gauche dite "décoloniale"**.


Sandew Hira
-------------

Dans le monde anglo-saxon, l’historien Sandew Hira, coordinateur du Réseau
Décolonial International, présentait le 26 février 2022
la Russie comme une victime de l’Occident, allant même jusqu’à comparer la
diabolisation dont Poutine ferait aujourd’hui l’objet dans les médias occidentaux
à celle des populations autochtones des Amériques chez les théologiens aux
premiers temps de la colonisation :ref:`13 <13_2023_02_27>` !


le média QG Décolonial
--------------------------

En France, le média QG Décolonial, bien connu pour par ailleurs pour son soutien
au régime de Bachar Al Assad, évoquait le 21 février 2022, quelques jours
avant l’invasion, "la menace d’un conflit majeur en Ukraine", en imputant la
responsabilité au "rapprochement entre l’Ukraine et l’OTAN et à la perspective
d’une installation de forces militaires occidentales à ses portes" et au
"Putsch de Maïdan mené par les forces les plus réactionnaires et anti-russes
d’Ukraine avec le soutien sans faille des Occidentaux", qui aurait poussé la
Russie à "déployer d’importants moyens militaires à la frontière avec ce pays" :ref:`14 <14_2023_02_27>`.

Et l’auteur du texte d’en conclure à la nécessité d’une dissolution de l’OTAN,
persuadé que celle-ci apaiserait la Russie et la conduirait à renoncer à ses
appétits guerriers…


.. _houria_bouteldja_2023_02_27:

Houria Bouteldja et le media controversé QG Décolonial
--------------------------------------------------------

Mais sans doute aurait-il été malhonnête de tenir ce média, relativement marginal et
proche de figures controversées, telle Houria Bouteldja :ref:`15 <15_2023_02_27>`, comme représentatif
du champ décolonial.


Boaventura de Sousa Santos
--------------------------------

Aussi me tournais-je vers les positions d’intellectuel·les issu·es de la sphère
académique, dans l’espoir d’y trouver des interventions à tout le moins plus
nuancées, mais pour aussitôt découvrir que plusieurs figures de proue des études
décoloniales, des universitaires parmi les plus influents d’Amérique latine, le
Portugais Boaventura de Sousa Santos et deux des membres emblématiques du groupe
Modernité/Colonialité :ref:`16 <16_2023_02_27>`, le Portoricain Ramon Grosfoguel
et l’Argentin Walter Mignolo, avaient été, eux aussi, des agents actifs de diffusion de la propagande
russe.

Sousa Santos, dans un article publié le 10 mars 2022, évoque ainsi la
stratégie de "provocation de la Russie et de neutralisation de l’Europe"
mise en place par les États-Unis : "l’expansion de la Russie fut provoquée
pour pouvoir ensuite être critiquée" :ref:`17 <17_2023_02_27>`.

Une thèse réitérée le 23 décembre 2022, dans un entretien où il affirme que
l’on assiste en Ukraine à "une guerre entre les Etats-Unis et la Russie" :ref:`18 <18_2023_02_27>`.


Ramon Grosfoguel
--------------------

Grosfoguel, de son côté, dans une interview accordée le 8 mars 2022, alla encore
plus loin, déclarant que les "États-Unis ont atteint l’objectif qu’ils
s’étaient fixés depuis plusieurs années", en orchestrant à l’aide de
"milices nazis" un "coup d’État international pour reprendre le contrôle
politique, économique et militaire de l’Europe." :ref:`19 <19_2023_02_27>`

Un mois plus tard, alors que l’Ukraine était sous les bombes et que les premières
images du massacre de Boutcha parvenaient aux yeux du monde, il évoquait à
nouveau, prétendant lutter contre la censure, "une guerre fabriquée aux Etats-Unis (…)
un génocide mené par des néo-nazis pour exterminer les Ukrainiens russophones (…)
et un coup d’Etat international mené contre la Chine et l’Europe, transformée en
néo-colonie américaine par l’intermédiaire de la marionnette Zelensky." :ref:`20 <20_2023_02_27>`

Walter Mignolo
-------------------

Mignolo, enfin, s’il n’a pas pris publiquement position sur l’invasion de
l’Ukraine de 2022 à ma connaissance, avait salué l’annexion de la Crimée
en 2014 sur son blog. Et dans un article publié en 2017, il se félicitait,
en l’assimilant à une forme de décolonisation en acte, de "l’émergence
de divers projets de dés-occidentalisation, parmi lesquels : la réémergence
politique de la Chine, le redressement de la Russie après l’humiliation de la fin
de l’URSS, qui lui a permis de s’opposer à l’occidentalisation de l’Ukraine
et de la Syrie, et la coopération de l’Iran avec la Chine et la Russie" :ref:`21 <21_2023_02_27>`.

Autant dire que je fus surpris en découvrant ces interventions, qui reproduisaient
jusque dans ses aspects les plus délirants le discours du Kremlin, tant il me
semblait évident que la guerre d’annexion menée par la Russie, vieille puissance
impériale et coloniale, aurait dû orienter la solidarité de ces auteurs vers
l’Ukraine.

La logique de l’anti-colonialisme ou de l’anti-impérialisme voudrait en effet
que les pays ou les peuples qui le subissent se solidarisent avec ceux qui le
subissent ailleurs, même si c’est sous la botte d’une puissance rivale de
celle qui les opprime eux-mêmes.


S’interroger sur les raisons qui ont poussé certain·es de ses plus éminent·es représentant·es de la pensée décoloniale à une telle complaisance envers le régime de Poutine
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

Avant d’esquisser des pistes pour comprendre ces propos, je tiens à souligner
que, de même qu’il serait abusif de parler de pensée décoloniale en général,
abstraction faite de toute hétérogénéité interne à ce courant, il ne saurait
être question de suggérer que tous·tes les auteur·ices se revendiquant du
champ des études décoloniales les ont adoptées, mais de s’interroger sur les raisons qui
ont poussé certain·es de ses plus éminent·es représentant·es à une telle
complaisance envers le régime de Poutine.

Il est de fait probable que d’autres figures aient pris fait et cause pour
l’Ukraine agressée, même si je n’ai pas trouvé de prises de position publiques
allant dans ce sens.

Il ne s’agit pas non plus, comme nous le verrons, de rejeter tous les
présupposés de cette pensée. À titre personnel, ayant longtemps vécu au
Mexique, j’y avais constaté que la structuration socio-raciale du pays, même si
elle n’était plus légalement ou constitutionnellement codifiée comme telle,
loin d’être purement résiduelle, témoignait à certains égards d’une
"colonialité" persistante.

En tant que traducteur, j’avais également remarqué, non sans regret, qu’à qualité
égale, il m’était beaucoup plus difficile de convaincre un·e éditeur·rice francophone
de traduire un ouvrage de sciences sociales quand il était hispanophone que
quand il était anglophone; je touchais ainsi du doigt à ces phénomènes de
colonialité du savoir et d’injustice épistémique pointé à juste titre par
les décoloniaux.

En tant qu’écologiste, je ne pouvais pas ignorer que les territoires sacrifiés
par l’extractivisme se trouvaient majoritairement dans des pays du Sud, ni que
dans d’anciennes colonies le régime plantationnaire hérité de l’esclavage, en
verrouillant les trajectoires économiques et écologiques de certains territoires,
continuait à avoir des effets dévastateurs sur la santé des habitant·es, comme
dans les Antilles françaises, gravement polluées par le chlordécone, pesticide
massivement épandu sur les bananerais pendant plusieurs décennies :ref:`22 <22_2023_02_27>`.

L’histoire de l’écologie elle-même n’était pas exempte de pratiques coloniales, de
nombreux parcs nationaux, en Amérique, en Afrique et en Asie, ayant été fondés
en excluant les populations autochtones qui les habitaient. Quant à l’histoire
des idées, je savais également à quel point la philosophie occidentale, et plus
encore moderne, tout au moins dans ses expressions dominantes, n’ayant cessé de
dévaloriser la Terre et d’ériger des systèmes de pensée anthropocentriques
tout en exacerbant le dualisme Nature/Culture, avait sa part de responsabilité
dans la catastrophe en cours.

Attirer l’attention sur les effets asymétriques persistants, sur les sociétés
comme sur les milieux, des différentes vagues de colonisation européennes et de
l’esclavage, mettre en lumière la double "fracture" coloniale et raciale
qui gît au cœur de la modernité capitaliste par-delà la simple division de
la société en classes me semble non seulement légitime, mais nécessaire.

À bien des égards, il parait pertinent, dans différents contextes historiques et
géographiques, d’établir une équivalence entre les couples "dominant/dominé"
et "centre/périphérie" d’un côté, et les couples "Nord global/Sud global",
"Occident/reste du monde" ou encore "Blanc/non-blanc (racisé)" de l’autre.

Une conception culturaliste des rapports de domination
=============================================================

Comment rendre compte, dès lors, de la réaction des penseurs décoloniaux
que j’ai cités face à la guerre en Ukraine ?

Par certains aspects, l’anti-américanisme de ces derniers, latino-américain·es
pour deux d’entre eux, s’explique par la responsabilité des États-Unis dans
la violence à laquelle leur continent à été soumis au XXème siècle, les conduisant
spontanément à voir partout la "main" de la puissance qui a soutenu tant
de dictatures, si besoin est par l’intervention militaire, dans leurs propres
pays.
Sur ce point, leurs positions ne diffèrent pas fondamentalement de celles de
campistes comme Jean-Luc Mélenchon lorsque celui-ci déclare qu’il ne croit pas
"à une attitude agressive de la Russie ou de la Chine. Seul le monde anglo-saxon
a une vision des relations internationales fondée sur l’agression." :ref:`23 <23_2023_02_27>`

Mais à cette vision essentialiste et unilatérale des relations internationales
s’ajoute chez eux, me semble-t-il, une forme d’essentialisme plus profond,
généralement absent des positions souverainistes comme celles de Mélenchon,
qu’ils puisent directement dans leur propre élaboration de la pensée
décoloniale : la tendance à ériger l’Occident moderne tel qu’il s’est
affirmé depuis 1492 et la conquête des Amériques jusqu’à nos jours,
ce que Grosfoguel nomme le "système-monde Européen/euro-nord-américain
moderne/colonial capitaliste/patriarcal" :ref:`24 <24_2023_02_27>`, en un bloc inchangé et pour ainsi
dire inchangeable. Sont ainsi rangés pêle-mêle sous la bannière de l’«
épistémé" moderne-coloniale "le capitalisme et le communisme, la théorie
politique des Lumières (libéralisme, républicanisme, Locke, Montesquieu),
l’économie politique (Smith) ainsi que son adversaire, le socialisme-communisme"
(Mignolo) :ref:`25 <25_2023_02_27>`. Quant aux tensions et contradictions internes à l’histoire
de l’Europe et de ses idées, elles sont tout simplement effacées, comme le
relève à juste titre Daniel Inclan, soulignant qu’il n’y a pas de place
dans leurs réflexions pour une "vision dialectique de l’Europe, celle-ci
étant présentée comme une unité, comme une substance maligne qui se répand
à travers le monde" :ref:`26 <26_2023_02_27>`.

Or ce cadre, à l’intérieur duquel l’analyse des situations concrètes semble
céder le pas à une métaphysique de l’histoire où un hyper-sujet tout-puissant
détient le quasi-monopole du mal dans le monde, est évidemment inopérant pour
saisir la spécificité et la complexité de la guerre en Ukraine, tout comme
il n’était guère probant pour comprendre la révolution et la guerre civile
syrienne. Un point qui a d’ailleurs été relevé avec justesse par le grand
écrivain syrien Yassin Al-Haj Saleh. Si celui-ci critique ici l’approche «
post-coloniale", qui renvoie à un courant de pensée légèrement différent,
et s’il parle de la Syrie, il me semble néanmoins intéressant de le citer
tant sa réflexion s’applique également très bien à l’approche de nos
auteurs et à l’Ukraine :

"La lecture post-colonialiste ne fournit pas d’outils pertinents pour expliquer
et comprendre l’histoire de la Syrie. Ni avant la révolution, ni après. La
marginalisation de la cause syrienne dans les milieux de la gauche internationale a
grandement partie liée, à mon avis, avec l’hégémonie du prisme post-colonial
ou, dans une langue plus classique, avec l’anti-impérialisme hérité des
années de guerre froide. Or la cause syrienne vient précisément révolutionner
la pensée libératrice mondiale par sa ‘complexité’, comme on l’entend
partout dire et répéter. ‘Complexe’, au sens où elle échappe à toute
exhaustivité analytique d’un quelconque cadre théorique donné. Or ce réel
complexe exige une réflexion complexe, qui dépasse les ‘salafismes’ (dans
le sens de poncifs traditionnels et rigides) de la gauche. Nous sommes au cœur
d’un processus dont on peut espérer qu’il participe à une révolution de
la théorie, faute de théorie de la révolution." :ref:`27 <27_2023_02_27>`

Bien sûr, certains éléments de la guerre en Ukraine et de ses effets ont pu
donner raison à nos auteurs. Ainsi, il est évident que l’accueil privilégié
dont bénéficièrent les réfugié·es ukrainien·nes, non seulement par rapport
aux réfugié·es syrien·nes, afghan·nes ou soudanais·ses avant elles et eux,
mais aussi par rapport aux étudiant·es africain·nes ou sri-lankais·ses qui
vivaient en Ukraine et furent souvent refoulé·es à la frontière polonaise,
était en partie lié au privilège racial que leur conférait leur blanchité. De
même, certains propos tenus sur les Ukrainien·nes fuyant leur pays, qualifié·es
d’"Européens de culture" et "d’immigration de grande qualité" :ref:`28 <28_2023_02_27>`, ou
encore la déclaration pour le moins polémique de Josep Borell, le vice-président
de la commission européenne, comparant l’Europe à un "jardin" et le
reste du monde à une "jungle" menaçante :ref:`29 <29_2023_02_27>`, avaient d’évidents accents
racistes et coloniaux, confirmant à bien des égards la persistance d’une «
hiérarchie ethno-raciale globale" (Grosfoguel) :ref:`30 <30_2023_02_27>`. Enfin, il est vrai que la
cause ukrainienne, contrairement à la cause syrienne quelques années auparavant
ou à la cause palestinienne, a bénéficié d’une visibilité médiatique
et d’un soutien diplomatique, économique et militaire important de la part
des puissances occidentales, symptomatiques d’une indignation à géométrie
variable en matière de respect du droit international.

Mais la critique légitime de ce double standard ne saurait à elle seule expliquer,
et encore moins justifier l’absence d’un soutien sans faille à la résistance
et la mobilisation massive de la société ukrainienne. Ce manque de solidarité
doit également se comprendre à l’aune des limites inhérentes à la pensée de
ces auteurs eux-mêmes. En opposant "la peau et les emplacements géo-historiques
des migrants du tiers-monde" à la "peau des ‘Européens de souche’ du
premier monde" :ref:`31 <31_2023_02_27>` (Mignolo), en affirmant que "l’épistémologie a une couleur"
:ref:`32 <32_2023_02_27>` (Grosfoguel) et que "le système-monde renvoie à une articulation spatiale
du pouvoir" :ref:`33 <33_2023_02_27>` (Mignolo) où le fondamentalisme eurocentrique et son prolongement
nord-américain, le "plus dangereux de la planète " (Grosfoguel) :ref:`34 <34_2023_02_27>`,
occupent une place centrale que rien ne semble pouvoir remettre en question,
ceux-ci donnent en effet l’impression de postuler une équivalence entre les
couples "dominant/dominé" et "centre/périphérie" et les couples «
Occident/Sud global" et "Blanc/non-Blanc". Or si cette thèse est à bien
des égards pertinente d’un point de vue historique (valide aujourd’hui encore
dans de nombreux contextes socio-politiques) elle devient en revanche extrêmement
problématique lorsqu’elle prend la forme d’une thèse essentialisante et
totalisante. Elle échoue alors à saisir l’historicité propre de nombreux
événements majeurs de notre temps, qui ne s’inscrivent pas nécessairement
dans la continuité de l’histoire coloniale et impériale européenne.

D’un point de vue historique, l’on pourrait bien sûr souligner que la situation
du monde n’a jamais correspondu complètement à cette thèse. Même à la fin
du XIXème siècle et au début du XXème siècle, alors qu’une bonne partie du
globe était sous domination européenne, que les lois Jim Crow et l’apartheid
racial s’imposaient aux États-Unis, consacrant le triomphe du suprémacisme
blanc, et que nombre de pays d’Amérique latine étaient gouvernés par des
élites postcoloniales désireuses de blanchir leur population par le recours à
l’immigration européenne, des pôles de domination indépendants subsistaient ou
s’épanouissaient, comme l’Empire Ottoman, dont les dernières années furent
marquées par le génocide des Arménien·nes, ou l’impérialisme japonais alors
en plein essor. Au cœur même de l’Europe, de nombreuses populations immigrées
blanches qui n’étaient pas issues de territoires colonisés faisaient l’objet
d’une xénophobie virulente allant parfois jusqu’à déclencher des massacres,
comme celui des ouvriers italiens à Aigues-Mortes en France en 1893 :ref:`35 <35_2023_02_27>`, sans même
parler de l’antisémitisme omniprésent, qui s’appuyait sur la racialisation
d’un "peuple" que rien ne distinguait, d’un point de vue phénotypique,
du reste de la population blanche, et qui allait conduire quelques décennies
plus tard à la Shoah. Cependant, entre la fin du XVème siècle et la moitié
du XXème, force est de reconnaître que la position défendue par Mignolo et
Grosfoguel est globalement juste, tant la domination de "l’Occident " sur
le monde a été massive.

C’est lorsqu’elle entend s’appliquer au monde contemporain dans sa totalité
que cette thèse devient plus fragile. À cet égard, nos auteurs gagneraient à
se remémorer les travaux d’historien·nes ayant montré qu’aux États-Unis,
les immigrant·es italien·nes ou irlandais·es avaient d’abord été «
racisé·es" avant d’être intégré·es à la sphère de la blanchité :ref:`36 <36_2023_02_27>`,
ou les travaux d’auteurs ayant soutenu que le concept de Sud global ne renvoie
pas nécessairement à un emplacement géographique, qu’il existe des "Nord
" à l’intérieur des "Sud", et vice versa. Même si pour la plupart
d’entre nous, il ne sera jamais possible de dissocier entièrement le mot Sud
du point cardinal auquel il fait originellement référence, ou le mot blanchité
de la couleur de peau correspondante, ce remaniement lexical et conceptuel aurait
peut-être permis à nos auteurs de "voir" et de reconnaître la souffrance et
la résistance des Ukrainien·nes agressé·es au lieu de prendre fait et cause
pour leur agresseur, tout aussi blanc et nordique d’ailleurs, mais dont la
rhétorique a manifestement le mérite à leurs yeux de s’en prendre à l’«
Occident collectif", ennemi désigné de Vladimir Poutine (nous y reviendrons).

"Simplisme historiographique, manichéisme permanent, essentialisme culturaliste,
provincialisme latinoaméricain" font partie des raisons qui expliquent cet
échec, auxquelles il faut ajouter une "apparente critique de l’eurocentrisme,
qui cache en réalité un occidentalisme tenace", comme l’ont bien mis en
évidence Pierre Gaussens et Gaya Makaran :ref:`37 <37_2023_02_27>`. Le paradoxe veut en effet que la
pensée de ces auteurs, dont l’une des vocations premières, parfaitement
légitime, était de critiquer l’"eurocentrisme" et de "provincialiser
l’Europe" :ref:`38 <38_2023_02_27>`, est souvent profondément eurocentrée et occidentalocentrée
lorsqu’elle se propose de comprendre le présent, la célébration béate
de l’Occident et de sa "mission civilisatrice" ayant laissé place à la
sempiternelle dénonciation de ses méfaits, sans jamais que sa centralité, même
lorsqu’elle ne correspond plus tout à fait aux évolutions du monde contemporain,
ne soit véritablement contestée. Il y a là-dedans comme une théologie politique
impensée : une cause première (en l’occurrence les États-Unis/l’Occident)
et des causes secondes qui en sont toujours le produit dérivé et réactif,
voire carrément l’objet passif.

En ceci, le campisme décolonial d’un Mignolo ou d’un Grosfoguel rejoint les
autres formes de campisme, qui tendent elles aussi à appréhender le monde au
prisme exclusif de l’influence américaine/occidentale. Or ne vaudrait-il pas
mieux le penser comme un enchevêtrement complexe et largement imprédictible
d’agentivités sociales, politiques et géopolitiques qui ne répondent pas
toutes à la puissance étatsunienne, qui ont leurs histoires et leurs dynamiques
propres? Admettre que d’autres peuples, d’autres États et d’autres puissances
sont, pour le meilleur et pour le pire, capables d’agir de leur propre chef
sans que "l’Occident" ou "l’Empire" ne les aient nécessairement
provoquées ou poussées à le faire ?

Cet occidentalocentrisme inversé se retrouve jusque dans la culture historique
de la gauche campiste, toutes sensibilités confondues. Si la longue histoire des
interventions américaines dans le monde, du coup d’État au Guatemala en 1954
à la guerre en Irak de 2003 en passant par la baie des Cochons à Cuba en 1961,
la guerre du Vietnam, le Chili de Pinochet dans les années 1970 et les Contras
nicaraguayennes des années 1980 est relativement bien connue et sans cesse
rappelée, une étrange amnésie semble en revanche y entourer l’histoire tout
aussi longue des interventions soviétiques dans nombre de ses périphéries, à
Berlin en 1953, à Budapest en 1956, à Prague en 1968 ou encore à Varsovie en
1980, sans même parler, dans le cas précis de l’Ukraine, de l’Holodomor :ref:`39 <39_2023_02_27>`
ou de la déportation des Tatars de Crimée :ref:`40 <40_2023_02_27>`, alors même que ces différents
événements font l’objet d’un grand nombre de travaux d’historien·nes. Une
méconnaissance qui apparaît clairement dans la pensée décoloniale de Grosfoguel
et de Mignolo, focalisée sur l’Europe de l’Ouest et l’Amérique, et par
conséquent incapable de faire place à la diversité des histoires coloniales
et de leurs legs. À cet égard, un décolonialisme polycentré pourrait être
une perspective féconde.

Certes, à la différence des empires coloniaux espagnol, britannique ou français,
qui se sont essentiellement développés "outre-mer", le colonialisme russe
a été un colonialisme d’"outre-terre" pour reprendre la judicieuse
distinction du géographe Michel Foucher :ref:`41 <41_2023_02_27>`. Ceci explique sans doute qu’il
soit moins aisément discernable, puisque les territoires conquis du XVIIème
siècle jusqu’à la fin de la seconde guerre mondiale l’ont été, par couches
successives, en périphérie immédiate du territoire-noyau initial. Et si certains
de ces territoires se sont émancipés de la tutelle soviétique après la chute
de l’Union, les séquelles de cette longue histoire coloniale restent vives,
notamment dans le Caucase et en Asie Centrale, où les populations subissent un
racisme persistant :ref:`42 <42_2023_02_27>`. Ce à quoi il faudrait ajouter que dans les premiers mois de
la guerre, ce sont les minorités ethniques de la Fédération de Russie, notamment
Bouriates ou Yakoutes, qui ont payé le plus lourd tribut sur le champ de bataille
ukrainien, alors que les classes moyennes moscovites ou saint-pétersbourgeoises,
blanches, étaient relativement épargnées.

Une dangereuse convergence avec la propagande des régimes autoritaires
============================================================================

Mais s’il n’en allait que d’un déficit de complexité dans l’analyse,
les choses ne seraient finalement pas si graves. Le problème, c’est que ce
réductionnisme provoque un aveuglément inquiétant quant à la nature et à
la diversité des menaces auxquelles nous sommes aujourd’hui confrontés,
quand il ne conduit pas à une complaisance ou une complicité active avec
des régimes autoritaires. À cet égard, il semble grand temps d’admettre
que nous ne vivons plus dans un système-monde monocentré, si tant est que
celui-ci ait jamais existé, où "l’Occident blanc" occuperait seul,
simplement traversé par des rivalités internes à sa dynamique et à son essence
supposées, la position hégémonique, mais dans un système-monde polycentré,
où la violence autoritaire, nationaliste et raciste peut surgir de toutes parts,
sans avoir été ourdie ou provoquée en dernière instance par l’OTAN, la CIA,
l’Europe ou quelque autre entité occidentale.

Bien sûr, les puissances occidentales continuent à jouir de nombreux privilèges
et à bénéficier d’échanges économiques et écologiques impérialistes et
inégaux, tout en assumant, pour la première d’entre elles, les États-Unis,
des ambitions hégémoniques persistantes. Bien sûr, l’ethno-nationalisme et le
suprémacisme blanc ne cessent de gagner du terrain dans l’Amérique de Trump et
dans la France de Zemmour où prolifèrent les angoisses de "Grand Remplacement
". Mais il faut également compter sur la menace du nationalisme grand-russe,
dont on mesure aujourd’hui en Ukraine (et hier en Tchétchénie ou en Syrie) la
violence sans limites, sur l’ethno-nationalisme et le suprémacisme hindous dans
l’Inde de Modi :ref:`43 <43_2023_02_27>`, d’ores et déjà meurtrier pour les musulman·es victimes
de pogroms ou pour les adivasis (nom par lequel sont désignées les populations
autochtones de l’Inde), ou encore sur l’ethno-nationalisme et le suprémacisme
Han en Chine, où un processus d’auto-racialisation est à l’œuvre au sein
de l’ethnie majoritaire, reléguant à un statut inférieur les populations
non-Han :ref:`44 <44_2023_02_27>`, victimes pour certaines d’entre elles, comme les Ouïghours, de crimes
contre l’humanité que d’aucun·es n’hésitent plus à qualifier de génocide.

Or la pensée des auteurs décoloniaux que nous avons évoqués, en dénonçant
"un objet fétichisé dénommé ‘Occident’, accusé de tout, et le
pouvoir occulte universel d’une caste rentière ‘occidentale’" (Vincent
Présumey) :ref:`45 <45_2023_02_27>`, tout en assimilant sans nuance l’engagement en faveur des «
droits humains (…) aux conceptions globales impériales et à la hiérarchie
ethno-raciale globale entre Européens et non-Européens" :ref:`46 <46_2023_02_27>` (Grosfoguel), entre
malheureusement en résonance avec l’idéologie et la propagande de ces régimes
politiques, qui tendent à présenter leur croisade contre l’Occident comme
un processus de décolonisation de l’ordre mondial. Ainsi Poutine, égrenant
dans son discours du 27 octobre 2022, abondamment commenté, la longue liste
des méfaits commis par "l’Occident" au cours de son histoire : la traite
d’esclaves, l’extermination des Indien·nes d’Amérique, l’exploitation
des ressources en Afrique et en Inde, les guerres coloniales, les bombardements
alliés de villes allemandes, la destruction de Hiroshima et de Nagasaki, les
guerres de Corée et du Vietnam, etc. Puis poursuivant en affirmant que "la
Russie n’acceptera jamais le diktat de l’Occident agressif, néocolonial" ni
les manigances des "Européens", de l’"OTAN", des "pays anglo-saxons
" et des "États-Unis" pour imposer au monde entier "totalitarisme,
despotisme et apartheid", "nationalisme et racisme", avant de conclure :
" ils ne veulent pas que nous soyons libres ; ils veulent que nous soyons une
colonie." :ref:`47 <47_2023_02_27>` Ainsi Sergueï Lavrov, son ministre des affaires étrangères,
renchérissant au cours d’une tournée diplomatique en Afrique : "notre pays
n’a pas terni sa réputation par les crimes sanglants du colonialisme et a
toujours sincèrement soutenu les Africains dans leur lutte pour la libération
du joug colonial." :ref:`48 <48_2023_02_27>` Ainsi Erdogan, l’autocrate turc, auteur d’un ouvrage
"où transparaît à chaque page la vision d’un monde injuste et binaire :
d’un côté l’Occident, les pays colonisateurs et impérialistes, aveuglés
par leurs privilèges ; de l’autre les opprimés musulmans." :ref:`49 <49_2023_02_27>` En Inde, les
philosophes Shaj Mohan et Divya Dwivedi ont mis en lumière la convergence entre
certaines théories postcoloniales et l’ultra-nationalisme hindou, unis dans
une même dénonciation du caractère "eurocentrique" des demandes de respect
des droits humains ou des revendications féministes :ref:`50 <50_2023_02_27>`.
Et bien sûr, en Chine, Xi Jinping et le PC, dont l’Occident et ses "valeurs"
sont désormais la cible désignée :ref:`51 <51_2023_02_27>`.

Pour revenir plus précisément sur la propagande du régime russe, il faut en
effet bien voir qu’elle joue sur deux tableaux. À l’adresse de la droite et
de l’extrême-droite, qui partage avec Poutine une même volonté de liquider
l’héritage de la modernité politique dans ses aspects émancipateurs et
démocratiques pour laisser place à un monde où toutes les dominations –
capitaliste, raciale, patriarcale, anthropocentrique, etc. – seront libres
de s’exprimer sans aucun contrepoids, et où toute opposition sera écrasée
par un régime de terreur, elle exalte la tradition et l’autorité, notamment
religieuse (avec la bénédiction du patriarche orthodoxe Kiril) tout en mettant
l’accent sur la décadence morale de l’Occident sous l’effet conjugué des
"envahisseurs" immigrés venus du Sud, de la dévirilisation induite par le
féminisme et les mouvements LGBTQI+, et last but not least, du "wokisme" et
de la "cancel culture" dont la Russie serait aujourd’hui victime.

Mais à l’adresse de nombreux pays du Sud et de certaines franges de
l’extrême-gauche, notamment décoloniale, elle se présente comme une puissance
anti-impérialiste – la Russie libérerait l’Ukraine et les Ukrainien·nes
de leur gouvernement à la botte de l’impérialisme américain depuis le «
coup d’État de Maïdan" en 2014 – et anti-colonialiste capable d’offrir
un contrepoids appréciable à l’hégémonie américaine. C’est évidemment
grossier quand on connaît la longue histoire, encore inachevée, du colonialisme
russe rappelée précédemment, mais cela marche jusqu’à un certain point. En
accusant l’OTAN d’être en dernier ressort le responsable de la guerre et en
s’opposant aux livraisons d’armes au nom d’un pacifisme aussi doucereux que
faussement vertueux, une certaine gauche semble en effet convaincue qu’un peu
d’"équilibre des puissances" et de "multipolarité" ne ferait pas de mal.

Ainsi, soit par naïveté, soit par enfermement dans des bulles idéologiques,
elle contribue à son insu à la barbarisation en cours du capitalisme et à
l’avènement du monde rêvé par l’extrême-droite et par toutes les forces
illibérales en présence, quand bien même elle le combat parfois honnêtement
par ailleurs. L’idéal d’un monde multipolaire n’a évidemment rien de
mauvais en soi, mais dans le contexte actuel, force est de constater qu’il
n’engagerait malheureusement pas de regain d’autonomie, de liberté et de
justice pour les peuples du monde, encore moins un relâchement de la pression
extractive et productive de plus en plus infernale exercée sur la Terre. Il
s’agirait plutôt d’un monde où les blocs géopolitiques les plus puissants
se reconnaîtraient le droit de préserver ou de rétablir en interne les
ordres sociaux les plus brutaux et inégalitaires, si besoin est en perpétrant
toutes sortes de crimes atroces sans que personne n’y trouve rien à redire
(ah la souveraineté !) tout en jouissant à leur périphérie d’une sphère
d’influence vassalisée et non contestée par les autres blocs. Bref, un monde
où chacun pourrait vaquer à ses petits massacres comme bon lui semble, et où
toute la fragile architecture normative des relations internationales érigée
depuis des décennies, fondée malgré ses immenses imperfections, inachèvements
et hypocrisies sur une référence de principe au respect du droit des peuples
à l’autodétermination, des droits humains et des libertés fondamentales,
serait liquidée. Une Syrie mondialisée sur fond d’effondrement des conditions
d’habitabilité de la Terre, voilà aujourd’hui le véritable horizon du «
non-alignement" et de la "multipolarité"

Dans un texte remarquable, La multipolarité, le mantra de l’autoritarisme :ref:`52 <52_2023_02_27>`, la
féministe indienne Kavita Krishnan a bien mis en lumière la convergence objective
qui s’opère entre certaines critiques de l’"Occident" venues de la gauche
et l’idéologie de régimes nationalistes et autoritaires qui cherchent à
discréditer toute référence à l’universalisme, à la démocratie et aux
droits humains – tout en incarcérant les militant·es qui les défendent,
accusé·es d’être des "agent·es de l’étranger" – au prétexte de
leur supposée "essence" occidentale, et donc coloniale :

"La multipolarité est la boussole qui oriente la compréhension de la gauche
dans les relations internationales. Tous les courants de la gauche en Inde et dans
le monde plaident depuis longtemps pour un monde multipolaire par opposition à
un monde unipolaire dominé par les États-Unis impérialistes.

Dans le même temps, la multipolarité est devenue la clé de voûte du langage
commun des fascismes et des autoritarismes mondiaux. C’est un cri de ralliement
pour les despotes, qui leur sert à déguiser leur guerre contre la démocratie
en guerre contre l’impérialisme. Le déploiement de la multipolarité pour
déguiser et légitimer le despotisme est rendu possible par l’acceptation
retentissante par la gauche mondiale de la multipolarité en tant qu’expression
bienvenue de la démocratisation anti-impérialiste des relations internationales.

En définissant sa réponse aux confrontations politiques au sein ou entre
les États-nations comme une option à somme nulle entre l’approbation de la
multipolarité ou de l’unipolarité, la gauche perpétue une fiction qui, même
à son meilleur, a toujours été trompeuse et inexacte. Mais cette fiction est
ouvertement dangereuse aujourd’hui, servant uniquement de dispositif narratif
et dramatique pour attribuer des rôles flatteurs aux fascistes et aux autoritaires.

Les conséquences malheureuses de l’engagement de la gauche en faveur d’une
multipolarité sans valeur sont illustrées de manière très frappante dans le
cas de sa réponse à l’invasion russe de l’Ukraine. La gauche mondiale et
indienne ont légitimé et amplifié (à des degrés divers) le discours fasciste
russe, en défendant l’invasion comme un défi multipolaire à l’impérialisme
unipolaire dirigé par les États-Unis."

Conclusion
===================

De cette convergence entre les positions de certains représentants de l’un
des courants les plus en vue de la gauche radicale contemporaine, spontanément
associé au camp de l’émancipation, et la rhétorique de certains des pires
régimes politiques de notre temps, que conclure ?

Il serait bien évidemment absurde d’en déduire que tout doit être rejeté dans
la pensée décoloniale en géneral.

En revanche, pour échapper au campisme, il semble indispensable que
des auteurs importants de ce champ, comme Grosfoguel et Mignolo, renoncent à leurs
tendances totalisantes et essentialisantes au profit d’approches circonscrites et
historicisées, historicisation qui pourrait elle-même conduire au décolonialisme
polycentré que j’évoquais, ce qui permettrait notamment de mieux penser,
par-delà la relation entre l’Europe de l’Ouest et ses anciennes colonies,
la situation spécifique des espaces post-soviétiques, comme le font par exemple
les chercheur.euses ukrainien·nes Adrian Ivakhiv :ref:`53 <53_2023_02_27>` et Hanna Perekhoda :ref:`54 <54_2023_02_27>`.

De ce point de vue, il pourrait être intéressant de s’inspirer des Zapatistes
du Mexique.

Engagé·es de longue date dans une lutte aux accents décoloniaux
contre le capitalisme et l’État mexicain, ils n’ont rien cédé au campisme,
et le 13 mars 2022, ils défilaient par milliers dans les villes du Chiapas en
soutien à la résistance ukrainienne et aux cris de "Poutine Dehors !". Dans un
deuxième temps, il conviendrait également d’admettre que toutes les dominations
politiques ne peuvent pas pour autant être pensées au prisme du concept de
"colonialité", que nombre d’entre elles s’inscrivent dans d’autres
dynamiques historiques. Enfin, en renonçant aux approches culturalistes de la
domination, il serait possible de se focaliser sur l’analyse des différences
proprement politiques entre les États qui s’affrontent aujourd’hui sur la
scène internationale et d’échapper ainsi au relativisme de tous·tes ceux
et celles qui semblent persuadé·es que "dans la nuit du capitalisme tardif,
tous les régimes sont gris." :ref:`55 <55_2023_02_27>`

Il faut bien sûr se garder de céder à la rhétorique du "monde libre"
brandie par des élites néolibérales hypocrites qui se posent en défenseures
de "valeurs" qu’elles ne cessent par ailleurs de bafouer, abandonnant des
personnes migrantes à une mort certaine en Méditerranée et parfois même des
peuples entiers, comme en Syrie, à leur anéantissement programmé. Mais tout en
restant vigilants face au cynisme de nos gouvernants, tentés de sanctuariser les
tendances les plus inégalitaires et écocides de nos sociétés en brandissant
la menace du "il y a pire ailleurs", il est primordial de reconnaître
que la guerre de libération nationale ukrainienne est aussi un affrontement
entre une dictature criminelle, qui n’esquisse pour tout avenir que la
multiplication des ruines et des charniers, et un régime où l’arbitraire
du capitalisme et de l’État est contrebalancé par des institutions et des
contre-pouvoirs (sociaux, médiatiques, intellectuels) permettant de garantir un
minimum de vitalité démocratique et d’État de droit, de telle sorte que des
percées émancipatrices y sont possibles et que l’avenir y est ouvert à la
contestation. L’historien Taras Bilous, auquel je laisserai le mot de la fin,
remarque à ce propos que s’il avait été Irakien en 2003, il aurait condamné
l’agression américaine mais ne se serait pas risqué à défendre le régime
de Sadam Hussein. En tant qu’Ukrainien, en 2023, il s’est en revanche engagé
sans hésitation dans les forces de défense territoriale pour défendre "la
fragile démocratie ukrainienne qui, loin d’être parfaite,mérite néanmoins
d’être protégée du régime para-fasciste de Poutine." :ref:`56 <56_2023_02_27>`


Notes
========

.. _1_2023_02_27:

1 Une guerre gênante : que faire lorsque la Russie attaque l’Ukraine mais que tu es de gauche ?
-----------------------------------------------------------------------------------------------------

https://lundi.am/Une-guerre-genante-que-faire-lorsque-la-Russie-attaque-l-Ukraine-mais-que-tu-es.

.. _2_2023_02_27:

2 Réinventons l'internationalisme (2/4) - La faillite d’un « anti-impérialisme à sens unique »
----------------------------------------------------------------------------------------------------

- https://blogs.mediapart.fr/pierre-dardot-et-christian-laval/blog/180322/reinventons-linternationalisme-24-la-faillite-d-un-anti-imperialisme-sens-uniq

.. _3_2023_02_27:

3 Pour une bonne synthèse concernant les positions de Jean-Luc Mélenchon en matière de politique internationale
-------------------------------------------------------------------------------------------------------------------

Pour une bonne synthèse concernant les positions de Jean-Luc
Mélenchon en matière de politique internationale, voir cette
remarquable note de blog de Jean-Yves Pranchère et ses nombreux liens :

- https://blogs.mediapart.fr/jean-yves-pranchere/blog/270322/l-inutilite-du-vote-utile.


.. _4_2023_02_27:

4 Pour une critique précise des positions de Chomsky sur la politique internationale
---------------------------------------------------------------------------------------------------------

Pour une critique précise des positions de Chomsky sur la politique internationale, voir la Lettre ouverte à Noam Chomsky publiée par un groupe d’universitaires
(https://blogs.berkeley.edu/2022/05/19/open-letter-to-noam-chomsky-and-other-like-minded-intellectuals-on-the-russia-ukraine-war/)
; l’article de l’écrivain Syrien Yassin Al-Haj
Saleh, Chomsky is no friend of the syrian revolution
(https://newlinesmag.com/review/chomsky-is-no-friend-of-the-syrian-revolution/)
et celui du chercheur français Jonathan Piron, "Y a-t-il un problème Chomsky
?", La Revue Nouvelle 2022/1 (N°1), p. 90-97.

.. _5_2023_02_27:

5 Selon Lula, l’ancien président brésilien, Volodymyr Zelensky est « aussi responsable » de la guerre que Vladimir Poutine
-------------------------------------------------------------------------------------------------------------------------------

- https://www.lemonde.fr/international/article/2022/05/05/selon-lula-volodymyr-zelensky-est-aussi-responsable-de-la-guerre-que-vladimir-poutine_6124832_3210.html.

.. _6_2023_02_27:

6 https://twitter.com/evoespueblo/status/1578423391828049924
---------------------------------------------------------------------

.. _7_2023_02_27:

7 Un voluptueux bourrage de crâne Pour reprendre l’expression de Pierre Rimbert et Serge Halimi
------------------------------------------------------------------------------------------------------

Pour reprendre l’expression de Pierre Rimbert et Serge Halimi, plumes du
Monde Diplomatique : https://www.monde-diplomatique.fr/2022/09/HALIMI/65016.

.. _8_2023_02_27:

8 Anti-impérialisme ou complicité avec l’agression russe ? Dixit Jean-Yves Pranchère
-----------------------------------------------------------------------------------------

https://esprit.presse.fr/actualites/jean-yves-pranchere/anti-imperialisme-ou-complicite-avec-l-agression-russe-43904.

.. _9_2023_02_27:

9 Ukraine : cette gauche qui n’a rien appris Florian Louis cité par Joseph Confavreux et Fabien Escalona
---------------------------------------------------------------------------------------------------------------

Florian Louis cité par Joseph Confavreux et Fabien Escalona dans leur
article "Ukraine, cette gauche qui n’a rien appris", le 27 novembre 2022.
https://www.mediapart.fr/journal/international/271122/ukraine-cette-gauche-qui-n-rien-appris.

.. _10_2023_02_27:

10 « Saigner la Russie »
---------------------------------------------------------------

- https://www.monde-diplomatique.fr/2022/06/HALIMI/64758.

.. _11_2023_02_27:

11 http://solitudesintangibles.fr/lanti-imperialisme-des-imbeciles-leila-al-shami/
----------------------------------------------------------------------------------------

.. warning:: ce site a disparu.

.. _12_2023_02_27:

12 Citons pêle-mêle les Polonais du parti Razem et l’historien ukrainien Taras
----------------------------------------------------------------------------------

Citons pêle-mêle:

- les Polonais du parti Razem et l’historien ukrainien Taras
  Bilous dans le Courrier d’Europe Centrale,
- Daria Saburova dans Contretemps,
- Denys Gorbach dans Lundimatin,
- Perrine Poupin dans Mouvements,
- Jean-Yves Pranchère dans Esprit,
- Edwy Plenel, Fabien Escalona et Joseph Confavreux dans Mediapart,
- le duo Dardot/Laval et le collectif internationaliste "La Cantine Syrienne"
  dans le blog de ce même journal,
- les "Brigades de Solidarité Editoriale" mises en place par les éditions
  Syllepse, ou bien encore Vincent Présumey dans Aplutsoc, pour n’en citer
  que quelques-un·es.

.. _13_2023_02_27:

13 A decolonial view of the war in Ukraine
----------------------------------------------------------------------

- https://din.today/news/a-decolonial-view-of-the-war-in-ukraine/

.. _14_2023_02_27:

14 qgdecolonial
----------------------

- https://qgdecolonial.fr/2022/02/21/edito-46-en-ukraine-comme-ailleurs-lotan-est-ladversaire-de-la-paix/
  voir quelques mois plus tard sur ce même site un appel incantatoire à la paix :
- https://qgdecolonial.fr/2022/10/10/plus-que-jamais-contre-la-guerre-plus-que-jamais-pour-la-paix-revolutionnaire/

.. note:: ces 2 sites sont désormais) inaccessibles (2024è

.. _15_2023_02_27:

15 **Le racket de l’amour révolutionnaire – Houria Bouteldja, le PIR et l’impasse de l’antiracisme raciste**
----------------------------------------------------------------------------------------------------------------

- https://www.europe-solidaire.org/spip.php?article38866


.. _16_2023_02_27:

16 Pour une bonne présentation de ce groupe et de ces idées, voir Claude
----------------------------------------------------------------------------

16 Pour une bonne présentation de ce groupe et de ces idées, voir Claude
Bourguignon et Philippe Colin, "De l’universel au pluriversel. Enjeux et
défis du paradigme décolonial".  Raison présente, 2016/3 (N°199), p. 99-108.

.. _17_2023_02_27:

17
-------

https://www.pagina12.com.ar/406933-el-lamentable-papel-de-europa-en-la-guerra-rusia-ucrania-y-l.

.. _18_2023_02_27:

18
------

https://www.rfi.fr/pt/programas/convidado/20221223-ucr%C3%A2nia-estamos-diante-de-uma-guerra-entre-os-eua-e-a-r%C3%BAssia.

.. _19_2023_02_27:

19
-----

https://observatoriodetrabajadores.wordpress.com/2022/03/18/ucrania-en-llamas-golpe-de-estado-internacional-de-eeuu-contra-rusia-entrevista-a-ramon-grosfoguel-miguel-angel-pirela/.

.. _20_2023_02_27:

20 https://www.youtube.com/watch?v=XBApUrQ4B10&ab_channel=LaIguanaTV.
------------------------------------------------------------------------

.. _21_2023_02_27:

21 https://www.journals.uchicago.edu/doi/full/10.1086/692552
----------------------------------------------------------------

.. _22_2023_02_27:

22 Voir sur ce point l’article de Malcom Ferdinand et Erwan Molinié
---------------------------------------------------------------------------

22 Voir sur ce point l’article de Malcom Ferdinand et Erwan Molinié, "Des
pesticides dans les outre-mer français", Écologie et politique, 2021/2 (N°63),
p. 81-94.

.. _23_2023_02_27:

23  Mélenchon « Ma ligne, c’est l’indépendance de la France » – Interview pour Le Figaro (2021)
-----------------------------------------------------------------------------------------------------

- https://melenchon.fr/2021/11/12/ma-ligne-cest-lindependance-de-la-france-interview-pour-le-figaro/.

.. _24_2023_02_27:

24 https://www.cairn.info/revue-multitudes-2006-3-page-51.htm
-----------------------------------------------------------------

.. _25_2023_02_27:

25 https://www.cairn.info/revue-mouvements-2013-1-page-181.htm.
----------------------------------------------------------------

.. _26_2023_02_27:

26 Daniel Inclan "La historia en disputa : el problema
-----------------------------------------------------------

26 Daniel Inclan "La historia en disputa : el problema
de la inteligibilidad del pasado", dans Piel blanca,
máscaras negras, crítica de la razón decolonial, p. 57.
(http://comunizar.com.ar/wp-content/uploads/Piel_blanca_mascaras_negras_Critica_de_l.pdf)

.. _27_2023_02_27:

27 Entretien avec Yassin Al Haj Saleh, « écrivain syrien sans terre sous ses pieds »
--------------------------------------------------------------------------------------------

- https://diacritik.com/2021/12/09/entretien-avec-yassin-al-haj-saleh-ecrivain-syrien-sans-terre-sous-ses-pieds-1-3/.

.. _28_2023_02_27:

28 Réfugiés ukrainiens : l’indignité derrière la solidarité
----------------------------------------------------------------

https://www.mediapart.fr/journal/international/010322/refugies-ukrainiens-l-indignite-derriere-la-solidarite.

.. _29_2023_02_27:

29 Josep Borrell, le maître jardinier de l’Europe se perd dans «la jungle»
--------------------------------------------------------------------------------

https://www.liberation.fr/international/josep-borrell-le-maitre-jardinier-de-leurope-se-perd-dans-la-jungle-20221019_2KOTPBOMSBC3RJLJRUJV7VMY2E/.

.. _30_2023_02_27:

30 Les implications des altérités épistémiques dans la redefinition du capitalisme global
---------------------------------------------------------------------------------------------

- https://www.cairn.info/revue-multitudes-2006-3-page-51.htm.

.. _31_2023_02_27:

31 Géopolitique de la sensibilité et du savoir. (Dé)colonialité, pensée frontalière et désobéissance épistémologique
--------------------------------------------------------------------------------------------------------------------------

- https://www.cairn.info/revue-mouvements-2013-1-page-181.htm.

.. _32_2023_02_27:

32 https://www.cairn.info/revue-multitudes-2006-3-page-51.htm.
-----------------------------------------------------------------------

.. _33_2023_02_27:

33 Les implications des altérités épistémiques dans la redefinition du capitalisme global
--------------------------------------------------------------------------------------------

- https://www.cairn.info/revue-multitudes-2001-3.htm

.. _34_2023_02_27:

34 reseaudecolonial
------------------------

- http://reseaudecolonial.org/wp-content/uploads/2016/09/Entretien-Ramon-Grosfoguel-RED.pdf.


.. warning:: ce site n'est plus accessible

.. _35_2023_02_27:

35 Gérard Noiriel, Le massacre des Italiens. Pluriel, 2018.
-------------------------------------------------------------------

.. _36_2023_02_27:

36 Nell Irvin Painter, Histoire des blancs. Max Milo, 2019.
------------------------------------------------------------------

.. _37_2023_02_27:

37
-----

37"Autopsia de una impostura intelectual", Critica de la razon decolonial,
opus cité, p. 21

.. _38_2023_02_27:

38 Pour reprendre le titre d’un texte célèbre de Dipesh Chakrabarty, penseur
-------------------------------------------------------------------------------------

du postcolonialisme dont rien n’indique, je le précise pour éviter tout
malentendu, qu’il ait diffusé les éléments de langage de la propagande
russe. Provincialiser l’Europe, la pensée postcoloniale et la différence
historique. Amsterdam, 2015.

.. _39_2023_02_27:

39 Holodomor
-----------------------

- https://fr.wikipedia.org/wiki/Holodomor#:~:text=Le%20jour%20comm%C3%A9moratif%20du%20Holodomor,qualifie%20de%20g%C3%A9nocide%20en%202022.

.. _40_2023_02_27:

40 Déportation des Tatars de Crimée
--------------------------------------------------------------------------------

- https://fr.wikipedia.org/wiki/D%C3%A9portation_des_Tatars_de_Crim%C3%A9e.

.. _41_2023_02_27:

41 Michel Foucher, Une guerre coloniale en Europe. Editions de l’Aube, 2022.
------------------------------------------------------------------------------------------

.. _42_2023_02_27:

42 Voir le remarquable article de Mathilde Goanec dans Mediapart
-----------------------------------------------------------------------

:https://www.mediapart.fr/journal/international/240722/de-bichkek-kazan-un-douloureux-reveil-postcolonial?.

.. _43_2023_02_27:

43 Inde : l’effrayante montée du nationalisme et de l’islamophobie
-------------------------------------------------------------------------

- https://mrmondialisation.org/inde-leffrayante-montee-du-nationalisme-et-de-lislamophobie/.

.. _44_2023_02_27:

44 Taïwan, Ouïghours : les dérives nationalistes de Xi Jinping
--------------------------------------------------------------------

- https://www.mediapart.fr/journal/international/111022/taiwan-ouighours-les-derives-nationalistes-de-xi-jinping.

.. _45_2023_02_27:

45 Vincent Présumey, publication du 30 septembre 2022 sur Facebook.
--------------------------------------------------------------------------

.. _46_2023_02_27:

46 Les implications des altérités épistémiques dans la redefinition du capitalisme global
---------------------------------------------------------------------------------------------

- https://www.cairn.info/revue-multitudes-2006-3-page-51.htm.

.. _47_2023_02_27:

47 Poutine a-t-il déclaré la guerre à l’Occident ?
--------------------------------------------------------------------

Pour une analyse détaillée du discours de Poutine, voir ce remarquable article
de Wiktor Stoczkowski dans Desk Russie : https://desk-russie.eu/2022/10/14/poutine-a-t-il-declare-la-guerre.html.


.. _48_2023_02_27:

48 Le chef de la diplomatie russe Sergueï Lavrov en tournée pour rassurer et soigner ses partenaires africains
----------------------------------------------------------------------------------------------------------------------

- https://www.lemonde.fr/afrique/article/2022/07/26/le-chef-de-la-diplomatie-russe-en-tournee-pour-rassurer-et-soigner-ses-partenaires-africains_6136152_3212.html.

.. _49_2023_02_27:

49 https://www.lhistoire.fr/dans-la-t%C3%AAte-de-recep-tayyip-erdogan.
-------------------------------------------------------------------------------

.. _50_2023_02_27:

50 «En Inde, être philosophe peut conduire à la mort»
----------------------------------------------------------

- https://www.mediapart.fr/journal/culture-idees/200518/en-inde-etre-philosophe-peut-conduire-la-mort.

.. _51_2023_02_27:

51 L’Occident, ennemi désigné de la Chine
-----------------------------------------------

- https://www.lemonde.fr/international/article/2022/10/14/l-occident-ennemi-designe-de-la-chine_6145809_3210.html.

.. _52_2023_02_27:

52 La multipolarité, le mantra de l’autoritarisme, par Kavita Krishnan
-----------------------------------------------------------------------------

- https://aplutsoc.org/2022/12/24/la-multipolarite-le-mantra-de-lautoritarisme-par-kavita-krishnan/.

.. _53_2023_02_27:

53 Decolonialism and the Invasion of Ukraine
-------------------------------------------------------------------------------------------

- https://www.e-flux.com/notes/457576/decolonialism-and-the-invasion-of-ukraine.

.. _54_2023_02_27:

54 Why Does Russia Still Think in Imperialist Categories and Does Not Recognize the Agentivity of Ukrainians? What Is Subaltern and What Does Colonialism Have to Do With It?
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

- https://zaborona.com/en/why-does-russia-still-think-in-imperialist-categories-and-does-not-recognize-the-agentivity-of-ukrainians-what-is-subaltern/.

.. _55_2023_02_27:

55 Ukraine : cette gauche qui n’a rien appris
-----------------------------------------------

F. Escalona et J. Confavreux,

- https://www.mediapart.fr/journal/international/271122/ukraine-cette-gauche-qui-n-rien-appris.


.. _56_2023_02_27:

56 Taras Bilous : « La démocratie ukrainienne, loin d’être parfaite, mérite d’être protégée du régime parafasciste de Poutine »
-------------------------------------------------------------------------------------------------------------------------------------

- https://courrierdeuropecentrale.fr/taras-bilous-une-grande-partie-de-la-gauche-prefere-une-approche-plus-imperialiste-exigeant-que-loccident-decide-pour-nous/.

Première parution dans lundimatin.

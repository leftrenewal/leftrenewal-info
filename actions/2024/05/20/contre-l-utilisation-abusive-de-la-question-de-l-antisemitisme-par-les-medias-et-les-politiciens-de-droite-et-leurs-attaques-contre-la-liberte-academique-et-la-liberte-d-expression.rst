.. index::
   pair: Leftrenewal ; Contre l’utilisation abusive de la question de l’antisémitisme par les médias et les politiciens de droite et leurs attaques contre la liberté académique et la liberté d’expression (2024-05-20)

.. _leftrenewal_fr_2024_05_20:

===================================================================================================================================================================================================================================================
2024-05-20 **Contre l’utilisation abusive de la question de l’antisémitisme par les médias et les politiciens de droite et leurs attaques contre la liberté académique et la liberté d’expression** par Ben Gidley, Daniel Mang & Daniel Randall
===================================================================================================================================================================================================================================================

- https://leftrenewal.org/fr/declarations/lettre-ouverte-2024-05-20/
- https://entreleslignesentrelesmots.wordpress.com/2024/05/24/contre-lutilisation-abusive-de-la-question-de-lantisemitisme-par-les-medias-et-les-politiciens-de-droite-et-leurs-attaques-contre-la-liberte-academique-et-la-liberte-dexpres/
- https://leftrenewal.org/signatories-open-letter-05-2024/
- https://leftrenewal.net/fr/french-version/
- :ref:`leftrenewal_fr_2023_12_10`
- :ref:`antisem:leftrenewal`

Source
==========

- https://leftrenewal.org/signatories-open-letter-05-2024/

Against the Abuse of the Issue of Antisemitism by Right Wing Media and Politicians
and Their Attacks on Academic Freedom and Freedom of Speech, by Ben Gidley,
Daniel Mang & Daniel Randall – 20 May 2024

Gegen den Missbrauch des Themas Antisemitismus durch rechte Medien und Politiker
und deren Angriffe auf akademische Freiheit und Meinungsfreiheit,
von Ben Gidley, Daniel Mang & Daniel Randall – 20. Mai 2024

- https://leftrenewal.org/de/erklaerungen/offener-brief-2024-05-20/

Texte
==========

Une déclaration que nous avons co-rédigée, publiée en décembre 2023,
`"Pour une gauche démocratique et internationaliste" <https://leftrenewal.net/fr/french-version/>`_,
a mis en évidence la mesure dans laquelle la gauche mondiale a un problème
d’antisémitisme.

**Nous avons vu comment l’antisémitisme a été amplifié et propagé au sein des mouvements
de solidarité avec la Palestine**.
Nous reconnaissons que les communautés juives de la diaspora se sentent
vulnérables et attaquées face à l’augmentation manifeste des incidents haineux
à leur encontre depuis le 7 octobre 2023.

La lutte contre l’antisémitisme (de droite et de gauche) est compromise
lorsque ceux qui prétendent être les alliés de la communauté juive lancent des
accusations irresponsables et injustes, qualifiant d’antisémites les critiques
légitimes à l’égard d’Israël.

Alors que certaines parties de l’extrême droite ont tenté (avec un certain
succès) de faire appel au sentiment de solidarité avec la Palestine et de
transformer la colère suscitée par les actions de l’État israélien en haine
des Juifs, **d’autres parties de l’extrême droite – dans la poursuite de
leurs propres objectifs – se sont fait passer pour des alliés d’Israël**.

En particulier, **nous avons vu comment le racisme antimusulman et les ressentiments
à l’égard des migrants** ont été nourris par cette partie de la droite, par
le biais de condamnations globales et infondées de communautés minoritaires
comme étant antisémites. Ces accusations ont été utilisées pour éroder
davantage le mur déjà fragile entre l’extrême droite et le conservatisme
plus modéré et centriste, les politiciens rivalisant pour paraître durs face
aux partisans présumés du terrorisme, en utilisant les craintes des juifs pour
leurs manœuvres électorales.

La diabolisation et la criminalisation de la dissidence, la réduction de la
liberté académique et les attaques contre l’université publique font partie
d’une dérive mondiale vers l’autoritarisme.

**Il est essentiel de contester l’antisémitisme dans le mouvement de solidarité
avec la Palestine**.

Dans le contexte politique actuel, on assiste également à une utilisation
abusive de la question de l’antisémitisme comme instrument d’une guerre
culturelle conservatrice autoritaire sur plusieurs fronts en Europe, en Israël,
dans les Amériques et en Océanie.

Des guerres culturelles similaires sont menées dans d’autres parties du
monde, et des liens étroits existent entre les différents courants culturels
antiféministes, les mouvements fondamentalistes religieux et les groupes
d’extrême droite dans différentes parties du monde.

La particularité de la guerre culturelle conservatrice dont nous parlons ici
est qu’elle fait l’amalgame entre une série de concepts académiques réels
et imaginaires (théorie critique, "marxisme culturel", intersectionnalité,
décolonialisme, "idéologie du genre", critical race theory), le radicalisme
étudiant, la présence de migrants et de musulmans en "Occident" et la figure
du terroriste et de l’antisémite.

Nombre de ses motifs et modes de pensée, tels que le concept de "marxisme
culturel" et la notion de puissants lobbies soutenus par les "élites
financières" travaillant à saper les "valeurs traditionnelles", sont
eux-mêmes antisémites.

Certaines organisations influentes dans les campements d’étudiants ont des
positions antisémites, comme la légitimation de la violence contre les civils
israéliens et le refus de reconnaître les droits collectifs des Juifs israéliens.

Cela ne signifie pas que tous les participants peuvent être qualifiés
d’antisémites.

Une robuste défense de la liberté d’expression, tant au sein des mouvements
que dans la société en général, est essentielle pour garantir que les voix
critiques puissent être entendues et que les points de vue réactionnaires soient
directement remis en question.

Au lieu de cela, de nombreuses universités ont adopté une approche lourde
et répressive à l’égard des étudiants protestataires. Malheureusement,
de nombreux médias grand public ont également joué un rôle très négatif,
en produisant des images simplistes et souvent diabolisantes des manifestations
– et de toute personne prônant le dialogue avec les manifestants.

Nous incriminons en particulier le groupe de presse Axel Springer, dont les
tabloïds Bild et BZ ont présenté les universitaires qui se sont élevés
contre la criminalisation des manifestations comme des partisans d’un "gang
d’étudiants", les accusant d’être hostiles aux Juifs et de banaliser la
violence aveugle – et ont personnalisé leur diffamation en publiant des photos
de ces universitaires dans le style des affiches Wanted.

Parmi les personnes visées figurent des universitaires qui ont une longue
expérience de la recherche et de la lutte contre l’antisémitisme (y compris
l’antisémitisme de gauche), tels que Peter Ullrich, membre du Centre de recherche
sur l’antisémitisme de Berlin, ou Michael Wildt, historien de l’Holocauste
de renommée internationale, qui met en évidence la fausseté des prétendues
préoccupations des tabloïds Springer en matière de racisme antijuif.

À une époque où les attaques contre les Juifs se multiplient, nous avons besoin
de clarté et de rigueur en ce qui concerne l’antisémitisme. À une époque où
différentes communautés sont prises pour cibles, nous avons besoin de dialogue,
et non de division, entre les minorités. À une époque où l’autoritarisme
s’accentue et où l’on agite des drapeaux sans raison, nous devons défendre
les espaces de dissidence et de pensée critique. À une époque de violence
déshumanisante, nous devons défendre la liberté académique et la liberté
d’expression.

Ben Gidley, Daniel Mang, Daniel Randall

Pour signer cette lettre ouverte, écrivez-nous à contact@leftrenewal.org.

Signataires
===============

- Abe Silberstein, Writer, Brooklyn, NY, USA
- Alexander Guschanski, Senior Lecturer in Economics, University of Greenwich, London, UK
- Andreas Hechler, Independent Researcher, Berlin, Germany
- Benno Herzog, Sociologist, University of Valencia, Spain
- Charlotte Rubin, Human Rights Lawyer, Writer, Brooklyn, NY, USA
- Denise Needleman, Stroud, UK
- Didier Epsztajn, Editor and Writer, Paris, France
- Dmitry Vilensky, Chto Delat Art Collective, Berlin, Germany
- Harsh Kapoor, Editor, Mainstream Weekly, India – France
- Jacky Assoun, France
- Jeremy Green, Stroud, UK
- Kavita Krishnan, Women’s Rights Activist and Writer, Delhi, India
- Martín Alonso Zarza, Retired Teacher, Madrid, Spain
- Michel Lanson, Retired Teacher, Paris, France
- Nissim Mannathukkaren, Professor, Department of International Development Studies, Dalhousie University, Halifax, Canada
- Philippe Corcuff, Professor of Political Science, Sciences Po, Lyon, France
- Rachel Garfield, Professor of Fine Arts, Royal College of Art, London, UK
- Susie Jacobs, Manchester, UK
- Uğur Yıldırım, Graduate Student, Koç University, Department of Philosophy, Istanbul, Turkey

- https://leftrenewal.org/signatories-open-letter-05-2024/

En complément possible
===========================


- https://entreleslignesentrelesmots.wordpress.com/2024/05/09/fureur-nationale/
  "Fureur nationale". Le point de vue du sociologue Peter Ullrich

